package com.audiobook.seeforward;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.audiobook.seeforward.activities.LandingScreenActivity;

public class SignUpActivity extends AppCompatActivity {
    TextView signUpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();
    }

    public void initView() {
        signUpBtn = findViewById(R.id.SignUp_btn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignUpActivity.this, LandingScreenActivity.class);
                startActivity(i);
            }
        });

    }
}
