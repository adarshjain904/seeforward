package com.audiobook.seeforward.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.audiobook.seeforward.Adapter.QuestionListAdapter;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.AudioData;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class BuyProgramActivity extends Activity implements QuestionListAdapter.SelectProgram, BillingProcessor.IBillingHandler {
    private String planId;

    public BuyProgramActivity() {
    }

    QuestionListAdapter questionListAdapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rc_question)
    RecyclerView rc_question;
    private Dialog dialog;
    private String allScore = "0";
    String str_audioId, str_audio, str_title;
    List<String> audioIDArrayList = new ArrayList<>();
    //callPaymentApi(audioId, audio, title, audioIdList);
    BillingProcessor bp;

    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_buy_program);
        ButterKnife.bind(this);

        mContext = this;
        //put your licence key here. You can find your key in Google Play Console -> Your App Name -> Services & APIs
        bp = new BillingProcessor(mContext, getString(R.string.license_key), this);
        bp.initialize();
        ProgressDailog();
        rc_question = findViewById(R.id.rc_question);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        rc_question.setLayoutManager(layoutManager);

        questionListAdapter = new QuestionListAdapter(mContext);
        rc_question.setAdapter(questionListAdapter);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            try {
                allScore = bundle.getString("allScore");
                planId = bundle.getString("plan_id");
                SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, allScore);

                if (Utility.isNetAvailable(mContext)) {
                    sendScoreToserver(allScore);
                } else {
                    Toast.makeText(mContext, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }
                // Log.i("rhl...S",allScore);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendScoreToserver(String score) {
        showDialog();


        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<AudioData> call = api.postTestScore(SharedPrefsManager.getInstance().getString(KEY_USER_ID), "22");
        call.enqueue(new Callback<AudioData>() {
            @Override
            public void onResponse(Call<AudioData> call, Response<AudioData> response) {
                Log.e("response", response.body().toString());
                cancleDialog();
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AudioData audioData = response.body();
                    // AudioData.UserDeatailsData userDeatailsData = audioData.getUser_details();
                    List<AudioData.AudioDataList> audioDataLists = audioData.getAudioDataLists();

                    if (audioDataLists != null && audioDataLists.size() > 0) {
                        questionListAdapter.addCustomerList(audioDataLists);
                        questionListAdapter.setSelectProgram(BuyProgramActivity.this);
                        Log.i("rhl...", "yes");
                    }
                } else {
                    Toast.makeText(mContext, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AudioData> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
                cancleDialog();
            }
        });
    }


    private void ProgressDailog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(mContext).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }

    @Override
    public void chooseProgram(String id, String audio, String title, List<String> audioIdList) {
        showPaymentDialog(id, audio, title, audioIdList);
    }

    public void showPaymentDialog(String audioId, String audio, String title, List<String> audioIdList) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", audioId);
        Button pay = dialog.findViewById(R.id.pay);
        pay.setOnClickListener(v -> {
                    dialog.dismiss();

                    if (Utility.isNetAvailable(mContext)) {
                        str_audioId = audioId;
                        str_audio = audio;
                        str_title = title;
                        audioIDArrayList = audioIdList;
                        //change your product id here

                        callPaymentApi(str_audioId, str_audio, str_title, audioIDArrayList, "XYZ");
                        // bp.purchase(QuestionsListActivity.this, "mental.health_strength1_4");

                    } else {
                        Toast.makeText(mContext, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                    // showCongratsDialog(id,audio,title);
                }
        );

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> dialog.dismiss()
        );
        dialog.show();
    }

    public void showCongratsDialog(String id, String audio, String title) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.congrats_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", id);
        Button cancel = dialog.findViewById(R.id.okay);
        cancel.setOnClickListener(v -> {
                    dialog.dismiss();
                    Intent intent = new Intent(mContext, MyProgrammesActivity.class);
                    startActivity(intent);
                    finish();
                }
        );
        dialog.show();
    }

    private void callPaymentApi(String audioId, String audio, String title, List<String> audioList, String g_order_id) {
        StringBuilder otherAudioId = new StringBuilder();
        for (int i = 0; i < audioList.size(); i++) {
            String id = audioList.get(i);
            if (!audioId.equals(id)) {
                otherAudioId.append(id);
                if (i < audioList.size() - 1) {
                    otherAudioId.append(",");
                }
            }
        }

        showDialog();



        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.postPaymentApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID),
                planId, "100",  SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("response", response.body().toString());
                cancleDialog();
                try {
                    if (response.body().string().contains("true")) {
                        SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                        showCongratsDialog(audioId, audio, title);
                    } else {
                        Toast.makeText(mContext, "Payment failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(mContext, t.toString(), Toast.LENGTH_SHORT).show();
                cancleDialog();
            }
        });
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        //if Multiple products
        /* switch (productId){
            case "buy_3_boost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"3");
                break;
            case "buy_10_bbost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"10");
                break;
            case "buy_20_boost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"20");
                break;
        }*/
        callPaymentApi(str_audioId, str_audio, str_title, audioIDArrayList, details.purchaseInfo.purchaseData.orderId);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
}
