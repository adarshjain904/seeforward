package com.audiobook.seeforward.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.audiobook.seeforward.Fragment.FeedBackFragment;
import com.audiobook.seeforward.Fragment.HelpsFragment;
import com.audiobook.seeforward.R;
import com.google.android.material.tabs.TabLayout;

public class HelpandSupportActivity extends AppCompatActivity {
    TabLayout tabNotificationLayout;
    FrameLayout frameLayout;
    Fragment fragment = null;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpand_support);
        init();
    }

    public void init() {
        tabNotificationLayout = findViewById(R.id.mynotification_tabLayout);
        backArrow = findViewById(R.id.back);
        frameLayout = findViewById(R.id.frameLayout);
        fragment = new HelpsFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

        // tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));
        tabNotificationLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        fragment = new HelpsFragment();
                        break;
                    case 1:
                         fragment = new FeedBackFragment();
                        break;
                }
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frameLayout, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //tabLayout.setTabTextColors(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
