package com.audiobook.seeforward.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.CalendarHistory;
import com.audiobook.seeforward.network.Constants;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarActivity extends AppCompatActivity {

    private String mUserId = "";

    private CalendarView calendarView;

    private TextView daysListenedTxtView;

    private RetrofitService retrofitService;
    List<Calendar> calendars;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        mUserId = SharedPrefsManager.getInstance().getString(Constants.KEY_USER_ID);

        retrofitService = RetrofitClient.getClient().create(RetrofitService.class);

        ImageView backBtn = findViewById(R.id.back_btn);
        backBtn.setOnClickListener(v -> onBackPressed());

        daysListenedTxtView = findViewById(R.id.days_listened_txt_view);
        calendarView = (CalendarView) findViewById(R.id.calendarView);

        calendarView.setOnDayClickListener(eventDay -> {
            Calendar clickedDayCalendar = eventDay.getCalendar();

            int year = clickedDayCalendar.get(Calendar.YEAR);
            int month = clickedDayCalendar.get(Calendar.MONTH);
            int day = clickedDayCalendar.get(Calendar.DAY_OF_MONTH);

            String date = year + "-" + (month + 1) + "-" + day;

            showListenCountDialog(date);
            ArrayList<Calendar> dated = new ArrayList<Calendar>();
            dated.add(clickedDayCalendar);

            calendarView.setSelectedDates(dated);
        });
        //setCurrentDate();
        if (Utility.isNetAvailable(this)) {
            getCalendarHistory(mUserId, getCurrentDate());
        } else {
            Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
        }
    }

    private void getCalendarHistory(String userId, String date) {
        retrofitService.getCalendarHistory(userId, date).enqueue(new Callback<CalendarHistory>() {
            @Override
            public void onResponse(Call<CalendarHistory> call, Response<CalendarHistory> response) {


                if (response.isSuccessful()) {
                    CalendarHistory calendarHistory = response.body();
                    if (calendarHistory != null) {
                        if (calendarHistory.getResponse().equals("true")) {
                            List<CalendarHistory.CalendarData> calendarDataList = calendarHistory.getCalendarDataList();

                            calendars = new ArrayList<>();

                            try {
                                if (calendarDataList.size() > 0) {
                                    for (int i = 0; i < calendarDataList.size(); i++) {
                                        Calendar calendar = Calendar.getInstance();

                                        String cDate = calendarDataList.get(i).getCreatedAt();
                                        cDate = cDate.substring(0,10);
                                        String[] dates = cDate.split("-");

                                        int year = Integer.parseInt(dates[0]);
                                        int month = Integer.parseInt(dates[1]);
                                        int day = Integer.parseInt(dates[2]);

                                        calendar.set(year, month - 1, day);
                                        calendars.add(calendar);
                                    }
                                    //adarsh 30/12/20
                                   // daysListenedTxtView.setText(String.valueOf(calendarDataList.size()));
                                    calendarView.setSelectedDates(calendars);
                                }
                              //  setCurrentDate();
                                //adarsh 30/12/20  add above comment line change my side
                                daysListenedTxtView.setText(String.valueOf(calendarHistory.getTotal()));
                            } catch (Exception e) {

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarHistory> call, Throwable t) {

            }
        });
    }

    private String getCurrentDate() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return simpleDateFormat.format(date);
    }

    private void showListenCountDialog(String date) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listen_count);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ProgressBar progressBar = dialog.findViewById(R.id.progress_bar);
        Button closeBtn = dialog.findViewById(R.id.dialog_close_btn);
        TextView listenCountTxtView = dialog.findViewById(R.id.listen_count_txt_view);

        progressBar.setVisibility(View.VISIBLE);

        if (Utility.isNetAvailable(CalendarActivity.this)) {
            getSelectedDateCount(date, listenCountTxtView, dialog, progressBar);
        } else {
            Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
        }

        closeBtn.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
    }

    private void getSelectedDateCount(String date, TextView listenCountTxtView, Dialog dialog, ProgressBar progressBar) {
        retrofitService.getListenCountByDate(mUserId, date).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                calendarView.setSelectedDates(calendars);
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getBoolean("status")) {
                            listenCountTxtView.setText(jsonObject.getString("data"));
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                  //  setCurrentDate();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

 /*  public void setCurrentDate(){
        Calendar c = Calendar.getInstance();
       ArrayList<Calendar> dated = new ArrayList<Calendar>();
       dated.add(c);

       calendarView.setSelectedDates(dated);
    }*/
}