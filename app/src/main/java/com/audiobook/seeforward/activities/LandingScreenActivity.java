package com.audiobook.seeforward.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.audiobook.seeforward.Fragment.AllQuestionsFragment;
import com.audiobook.seeforward.Fragment.DashboardFragment;
import com.audiobook.seeforward.Fragment.HistoryFragment;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.login.LogInActivity;
import com.audiobook.seeforward.login.SelectionActivity;
import com.audiobook.seeforward.models.Audio;
import com.audiobook.seeforward.models.CheckAudioStatus;
import com.audiobook.seeforward.network.Constants;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.profile.EditProfileActivity;
import com.audiobook.seeforward.profile.ProfileActivity;
import com.audiobook.seeforward.utiles.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_EMAIL;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_NAME;

public class LandingScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private Dialog dialog, mDialog2;
    private boolean showDashboard = false;
    private FragmentManager fragmentManager;
    // private String mAllScore = "";
    private RetrofitService retrofitService;
    SharedPrefsManager sharedPrefsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_screen);

        fragmentManager = getSupportFragmentManager();

        retrofitService = RetrofitClient.getInterface();
        sharedPrefsManager = SharedPrefsManager.getInstance();

        ProgressDailog();
        setHeader();

        if (!sharedPrefsManager.getString(Utility.PREF_DAY_DATA).equals("0")) {
            showInfoDialog();
        } else if (TextUtils.isEmpty(sharedPrefsManager.getString(Utility.PREF_SCORE)) || "0".equalsIgnoreCase(sharedPrefsManager.getString(Utility.PREF_SCORE))) {
            showInfoDialog();
        } else {
            loadFragment(new DashboardFragment());
        }
       /* FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();*/
    }

    private void setHeader() {
        drawer = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = (TextView) hView.findViewById(R.id.userName);
        TextView tv_email = (TextView) hView.findViewById(R.id.tv_email);
        ImageView editProfile = (ImageView) hView.findViewById(R.id.editProfile);
        ImageView close = (ImageView) hView.findViewById(R.id.close);
        ImageView userimage = (ImageView) hView.findViewById(R.id.userimage);

        nav_user.setText(SharedPrefsManager.getInstance().getString(KEY_USER_NAME));
        tv_email.setText(SharedPrefsManager.getInstance().getString(KEY_USER_EMAIL));

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.usereditimage)
                .error(R.drawable.usereditimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH).dontAnimate()
                .dontTransform();

        Glide.with(this).load(Constants.ImageBaseurl + SharedPrefsManager.getInstance().getString(KEY_USER_IMAGE))
                .apply(options).into(userimage);
        Log.e("user img full", Constants.ImageBaseurl + SharedPrefsManager.getInstance().getString(KEY_USER_IMAGE));
        Log.e("user img", SharedPrefsManager.getInstance().getString(KEY_USER_IMAGE));

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setHomeAsUpIndicator(R.drawable.navigation_bar);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        close.setOnClickListener(v -> drawer.closeDrawer(GravityCompat.START));

        editProfile.setOnClickListener(v -> {
            drawer.closeDrawer(GravityCompat.START);
          /*  fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new EditProfileFragment()).addToBackStack(null);
            fragmentTransaction.commit();
           */
            Intent intent = new Intent(LandingScreenActivity.this, EditProfileActivity.class);
            startActivity(intent);
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            if (TextUtils.isEmpty(sharedPrefsManager.getString(Utility.PREF_SCORE)) || "0".equalsIgnoreCase(sharedPrefsManager.getString(Utility.PREF_SCORE))) {
                showInfoDialog();
            } else {
                loadFragment(new DashboardFragment());
            }
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            Intent profileIntent = new Intent(LandingScreenActivity.this, ProfileActivity.class);
            startActivity(profileIntent);
        } else if (id == R.id.nav_calender) {
            if (TextUtils.isEmpty(sharedPrefsManager.getString(Utility.PREF_SCORE)) || "0".equals(sharedPrefsManager.getString(Utility.PREF_SCORE))) {
                openDialog();
            } else {
                Intent intent = new Intent(LandingScreenActivity.this, CalendarActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_programmes) {
            if (TextUtils.isEmpty(SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE)) || "0".equals(sharedPrefsManager.getString(Utility.PREF_SCORE))) {
                openDialog();
            } else {
                Intent programmeIntent = new Intent(LandingScreenActivity.this, MyProgrammesActivity.class);
                startActivity(programmeIntent);
            }
        } else if (id == R.id.nav_history) {
            loadFragment(new HistoryFragment());
        } else if (id == R.id.nav_help) {
            Intent i = new Intent(LandingScreenActivity.this, HelpandSupportActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            showLogoutDialog();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showLogoutDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button no = dialog.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button yes = dialog.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                SharedPrefsManager.getInstance().clearPrefs();

                Intent intent = new Intent(LandingScreenActivity.this, SelectionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void ProgressDailog() {
        dialog = new Dialog(LandingScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = dialog.findViewById(R.id.loder);
        Glide.with(LandingScreenActivity.this).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.landing_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            if ("0".equals(sharedPrefsManager.getString(Utility.PREF_SCORE)) || TextUtils.isEmpty(SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE))) {
                openDialog();
            } else {
                Intent programmeIntent = new Intent(LandingScreenActivity.this, MyProgrammesActivity.class);
                startActivity(programmeIntent);
            }
            return true;
        }
        if (id == android.R.id.home) {
            if ("0".equals(sharedPrefsManager.getString(Utility.PREF_SCORE)) || TextUtils.isEmpty(SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE))) {
                openDialog();
            } else {
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LandingScreenActivity.this);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Please answer the question first, to get in the app.");
        // Set behavior of negative button

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
                arg0.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        try {
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showUnlockDialog(String title) {
        Dialog dialog = new Dialog(LandingScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_programme_unlocked);

        Button closeBtn = dialog.findViewById(R.id.dialog_close_btn);
        closeBtn.setOnClickListener(v -> dialog.dismiss());

       /* TextView prograrammeUnlockTv = dialog.findViewById(R.id.programme_unlock_txt_view);

        prograrammeUnlockTv.setText("Congratulation, you've unlocked a new programme '" + title + "'");*/

        dialog.show();
    }

    public void showPaymentDialog(String audioId, String amount, List<Audio> audioList) {
        final Dialog dialog = new Dialog(LandingScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", audioId);
        Button pay = dialog.findViewById(R.id.pay);
        pay.setOnClickListener(v -> {
                    dialog.dismiss();

                    if (Utility.isNetAvailable(LandingScreenActivity.this)) {
                        callPaymentApi(audioId, amount, audioList);
                        // bp.purchase(QuestionsListActivity.this, "mental.health_strength1_4");

                    } else {
                        Toast.makeText(LandingScreenActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                    // showCongratsDialog(id,audio,title);
                }
        );

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void callPaymentApi(String planId, String amount, List<Audio> audioList) {


        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.postPaymentApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID),
                planId, amount, SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE));
        dialog.show();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("response", response.body().toString());
                dialog.dismiss();
                /*try {
                    if (response.body().string().contains("true")) {
                        SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                        showCongratsDialog(planId);
                    } else {
                        Toast.makeText(LandingScreenActivity.this, "Payment failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                showCongratsDialog(planId);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LandingScreenActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    public void showCongratsDialog(String id) {
        final Dialog dialog = new Dialog(LandingScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.congrats_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button cancel = dialog.findViewById(R.id.okay);
        cancel.setOnClickListener(v -> {
                    dialog.dismiss();
                    startActivity(new Intent(LandingScreenActivity.this, MyProgrammesActivity.class));
                }
        );
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Fragment fragment;

        if (!sharedPrefsManager.getString(Utility.PREF_DAY_DATA).equals("0")) {
            if (!mDialog2.isShowing())
                showInfoDialog();
        } else if (TextUtils.isEmpty(sharedPrefsManager.getString(Utility.PREF_SCORE)) || "0".equalsIgnoreCase(sharedPrefsManager.getString(Utility.PREF_SCORE))) {
            if (!mDialog2.isShowing())
                showInfoDialog();
        } else {
            loadFragment(new DashboardFragment());

        }

    }

    public void showInfoDialog() {
        loadFragment(new AllQuestionsFragment());
        mDialog2 = new Dialog(this);
        mDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog2.setCancelable(false);
        mDialog2.setCanceledOnTouchOutside(false);

        mDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog2.setContentView(R.layout.info_dailog);

        LinearLayout cancel = mDialog2.findViewById(R.id.okay);

        mDialog2.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(LandingScreenActivity.this, "ok done", Toast.LENGTH_SHORT).show();
                mDialog2.dismiss();
                mDialog2.hide();

            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

 /*   private void showProgressDialog() {
        dialog.show();
    }

    private void cancelDialog() {
        dialog.dismiss();
    }

    public boolean compareDates(String d1, Date currentDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date expireDate = sdf.parse(d1);

            String currentDateString = sdf.format(currentDate);

            // Date object is having 3 methods namely after,before and equals for comparing
            // after() will return true if and only if date1 is after date 2
            if (expireDate != null) {
                if (expireDate.before(currentDate) && !d1.equals(currentDateString)) {
                    System.out.println("Expire Date is before Current Date or not equal to current date");
                    return true;
                } else {
                    return false;
                }
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean isDateExpired(String d1, Date currentDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date expireDate = sdf.parse(d1);

            String currentDateString = sdf.format(currentDate);

            // Date object is having 3 methods namely after,before and equals for comparing
            // after() will return true if and only if date1 is after date 2
            if (expireDate != null) {
             *//*   if (expireDate.before(currentDate) && !expireDate.equals(currentDate)) {
                    System.out.println("Expire Date is before Current Date");
                    return true;
                } else {
                    return false;
                }
            }*//*
                if (expireDate.after(currentDate) || d1.equals(currentDateString)) {
                    System.out.println("Expire Date is after or equals Current Date");
                    return false;
                } else {
                    return true;
                }
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return false;
    }
     private void unlockProgramme(String expiryDate, String nextAudioHistoryId, String title) {
        retrofitService.unlockAnotherProgram(expiryDate, nextAudioHistoryId).enqueue(new Callback<CheckAudioStatus>() {
            @Override
            public void onResponse(Call<CheckAudioStatus> call, Response<CheckAudioStatus> response) {
                if (response.isSuccessful()) {
                    CheckAudioStatus checkAudioStatus = response.body();
                    if (checkAudioStatus != null) {
                        if (checkAudioStatus.getResponse().equals("true")) {
                            showUnlockDialog(title);
                        }
                        // Toast.makeText(LandingScreenActivity.this, checkAudioStatus.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckAudioStatus> call, Throwable t) {
                Toast.makeText(LandingScreenActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }
    */
}