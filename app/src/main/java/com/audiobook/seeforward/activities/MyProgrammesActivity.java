package com.audiobook.seeforward.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.audiobook.seeforward.Adapter.AdapterDrawerItem;
import com.audiobook.seeforward.Adapter.MyPlanListAdapter;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.login.SplashScreenActivity;
import com.audiobook.seeforward.models.Audio;
import com.audiobook.seeforward.models.ProgrammWithAudioList;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class MyProgrammesActivity extends AppCompatActivity implements PurchasesUpdatedListener, ConsumeResponseListener {

    RetrofitService apiInterface;
    String user_id;
    RecyclerView rc_plan;
    ProgressBar progress_bar;
    ExpandableListView elvDrawerItems;
    MyPlanListAdapter myPlanListAdapter;

    List<String> myPlansList = new ArrayList<>();
    BillingClient billingClient;

    private SkuDetails mSkuDetails;
    String mPlanId, mAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_programmes);

        ImageView backBtn = findViewById(R.id.back_btn);
        backBtn.setOnClickListener(v -> onBackPressed());

        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);

        rc_plan = findViewById(R.id.rc_plan);
        progress_bar = findViewById(R.id.progress_bar);
        elvDrawerItems = findViewById(R.id.expandable_list_programms);

        elvDrawerItems.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    elvDrawerItems.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rc_plan.setLayoutManager(layoutManager);
        rc_plan.setVisibility(View.GONE);
        myPlanListAdapter = new MyPlanListAdapter(this);
        rc_plan.setAdapter(myPlanListAdapter);

        if (Utility.isNetAvailable(this)) {
            getProgrammWithAudio();
        } else {
            Toast.makeText(this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void getProgrammWithAudio() {
        progress_bar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.getAudioList(SharedPrefsManager.getInstance().getString(KEY_USER_ID));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progress_bar.setVisibility(View.GONE);
                if (response.body() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.e("response", response.body().toString());
                        if (jsonObject != null & jsonObject.getBoolean("status")) {
                            Type type = new TypeToken<ArrayList<ProgrammWithAudioList>>() {
                            }.getType();
                            ArrayList<ProgrammWithAudioList> programmWithAudioLists = new Gson().fromJson(jsonObject.getJSONArray("data").toString(), type);
                            if (programmWithAudioLists != null) {
                                elvDrawerItems.setAdapter(new AdapterDrawerItem(MyProgrammesActivity.this, programmWithAudioLists));
                                for (int i = 0; i < programmWithAudioLists.size(); i++) {
                                    myPlansList.add("plan_id_" + programmWithAudioLists.get(i).getPlanId());
                                }
                            }

                            setupBillingClient();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MyProgrammesActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MyProgrammesActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        myPlanListAdapter.makeClickFalse(false);
    }

    public void showPaymentDialog(String planId, String amount, List<Audio> audioList) {
        final Dialog dialog = new Dialog(MyProgrammesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mAmount = amount;
        mPlanId = planId;

        Button pay = dialog.findViewById(R.id.pay);
        pay.setOnClickListener(v -> {
                    dialog.dismiss();

                    if (Utility.isNetAvailable(MyProgrammesActivity.this)) {
                        //adarsh 8/1/21 below line comment and add payment method
                        //callPaymentApi(audioId, amount, audioList);
                        //my comment
                        /*  if (mSkuDetails != null) {
                            BillingFlowParams billingFlowParams = BillingFlowParams
                                    .newBuilder()
                                    .setSkuDetails(mSkuDetails)
                                    .build();
                            billingClient.launchBillingFlow(MyProgrammesActivity.this, billingFlowParams);
                        } else {
                            Toast.makeText(MyProgrammesActivity.this, "Google play console Error", Toast.LENGTH_SHORT).show();
                        }*/
                        loadAllSKUs();
                        // bp.purchase(QuestionsListActivity.this, "mental.health_strength1_4");

                    } else {
                        Toast.makeText(MyProgrammesActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                    // showCongratsDialog(id,audio,title);
                }
        );

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> dialog.dismiss()
        );
        dialog.show();
    }

    public void showSubscribeDialog(String planId) {
        final Dialog dialog = new Dialog(MyProgrammesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.subcribe_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button pay = dialog.findViewById(R.id.pay);
        pay.setOnClickListener(v -> {
                    dialog.dismiss();
                    callSubscribeApi(planId);
                }
        );

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> dialog.dismiss()
        );
        dialog.show();
    }


    private void setupBillingClient() {
        billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    //Date 27/1
                    Purchase.PurchasesResult pr = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
                    List<Purchase> pList = pr.getPurchasesList();
                    for (Purchase iitem : pList) {
                        ConsumeParams consumeParams = ConsumeParams.newBuilder().setPurchaseToken(iitem.getPurchaseToken()).build();
                        billingClient.consumeAsync(consumeParams, MyProgrammesActivity.this);
                    }
                } else {
                    Toast.makeText(MyProgrammesActivity.this, "Google play Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Toast.makeText(MyProgrammesActivity.this, "Server Disconnected", Toast.LENGTH_SHORT).show();
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    private void loadAllSKUs() {
        if (billingClient.isReady()) {
            //Toast.makeText(MainActivity.this, "billingclient ready", Toast.LENGTH_SHORT).show();
            SkuDetailsParams params = SkuDetailsParams.newBuilder()
                    .setSkusList(myPlansList)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();

            billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && !skuDetailsList.isEmpty()) {
                        for (int i = 0; i < skuDetailsList.size(); i++) {
                            if (skuDetailsList.get(i).getSku().equals("plan_id_" + mPlanId)) {
                                mSkuDetails = skuDetailsList.get(i);
                            }
                        }

                        if (mSkuDetails != null) {
                            BillingFlowParams billingFlowParams = BillingFlowParams
                                    .newBuilder()
                                    .setSkuDetails(mSkuDetails)
                                    .build();
                            billingClient.launchBillingFlow(MyProgrammesActivity.this, billingFlowParams);
                        } else {
                            Toast.makeText(MyProgrammesActivity.this, "Google play console Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } else
            Toast.makeText(MyProgrammesActivity.this, "Your payment method has not been set up yet.", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        int responseCode = billingResult.getResponseCode();
        if (responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
        } else if (responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
        } else {
        }
    }

    @Override
    public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {

    }


    private void handlePurchase(Purchase purchase) {
        if (purchase.getSkus().equals("plan_id_" + mPlanId) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder().setPurchaseToken(purchase.getPurchaseToken()).build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
                    @Override
                    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            Toast.makeText(MyProgrammesActivity.this, "Purchase Acknowledged", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            ///mSharedPreferences.edit().putBoolean(getResources().getString(R.string.pref_remove_ads_key), true).commit();
            ///setAdFree(true);
            Toast.makeText(this, "Purchase done. you are now a premium member.", Toast.LENGTH_SHORT).show();


            callPaymentApi(mPlanId, mAmount);
        }
    }

    private void callPaymentApi(String planId, String amount) {
        progress_bar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.postPaymentApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID),
                planId, amount, SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progress_bar.setVisibility(View.GONE);
                //adarsh 3/1/21 open comment below try to catch
                try {
                    if (response.body().string().contains("true")) {

                        //  SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                        showCongratsDialog(planId);
                    } else {
                        Toast.makeText(MyProgrammesActivity.this, "Payment failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //adarsh 3/1/21 comment below two line
                // SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                //showCongratsDialog(planId);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MyProgrammesActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);
            }
        });
    }

    private void callSubscribeApi(String planId) {
        progress_bar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.subscribeFreePackagesApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID), planId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body().string().contains("true")) {
                        getProgrammWithAudio();
                    } else {
                        progress_bar.setVisibility(View.GONE);
                        Toast.makeText(MyProgrammesActivity.this, "failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    progress_bar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MyProgrammesActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);
            }
        });
    }


    public void showCongratsDialog(String id) {
        final Dialog dialog = new Dialog(MyProgrammesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.congrats_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", id);
        Button cancel = dialog.findViewById(R.id.okay);
        cancel.setOnClickListener(v -> {
                    dialog.dismiss();
                    getProgrammWithAudio();
                }
        );
        dialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onRestart=-=","onRestart");
        checkdays();
    }

    private void checkdays() {
        RetrofitClient.getInterface().checkShowQuestion(SharedPrefsManager.getInstance().getString(KEY_USER_ID)).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
             //   Log.d("response", response.body().toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    if (jsonObject.getBoolean("status")) {
                        SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, jsonObject.getString("data"));
                        finish();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
        // progressDialog.dismiss();
    }
}
