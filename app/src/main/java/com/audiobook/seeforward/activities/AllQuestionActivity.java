package com.audiobook.seeforward.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.AllQuestion;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.utiles.Utility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllQuestionActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.ans1)
    TextView ans1;
    @BindView(R.id.ans2)
    TextView ans2;
    @BindView(R.id.ans3)
    TextView ans3;
    @BindView(R.id.ans4)
    TextView ans4;
    @BindView(R.id.question)
    TextView question;
    @BindView(R.id.next)
    TextView next;
    int index = 1;
    public List<AllQuestion.Data> dataList;
    @BindView(R.id.back3)
    RelativeLayout back3;
    @BindView(R.id.back4)
    RelativeLayout back4;
    @BindView(R.id.back2)
    RelativeLayout back2;
    @BindView(R.id.back1)
    RelativeLayout back1;
    @BindView(R.id.count1)
    TextView count1;
    @BindView(R.id.count2)
    TextView count2;
    @BindView(R.id.count3)
    TextView count3;
    @BindView(R.id.count4)
    TextView count4;
    private boolean isSelected = false;
    private int allScore = 0;
    private String mPreviousScore = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_question);
        ButterKnife.bind(this);

        if (Utility.isNetAvailable(this)) {
            getAllQuestions();
        } else {
            Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
        }
    }

    private void getAllQuestions() {

        progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<AllQuestion> call = api.getQuestions();
        call.enqueue(new Callback<AllQuestion>() {
            @Override
            public void onResponse(Call<AllQuestion> call, Response<AllQuestion> response) {
                Log.e("response", response.body().toString());
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AllQuestion allQuestion = response.body();
                    dataList = allQuestion.getData();
                    question.setText(allQuestion.getData().get(0).getQuestion());
                    if (allQuestion.getData().get(0).getChoose_option().equalsIgnoreCase("2")) {
                        ans1.setText(allQuestion.getData().get(0).getAnswer1());
                        ans2.setText(allQuestion.getData().get(0).getAnswer2());
                        back3.setVisibility(View.GONE);
                        back4.setVisibility(View.GONE);
                    } else {
                        ans1.setText(allQuestion.getData().get(0).getAnswer1());
                        ans2.setText(allQuestion.getData().get(0).getAnswer2());
                        ans3.setText(allQuestion.getData().get(0).getAnswer3());
                        ans4.setText(allQuestion.getData().get(0).getAnswer4());
                    }
                } else {
                    Toast.makeText(AllQuestionActivity.this, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllQuestion> call, Throwable t) {
                Toast.makeText(AllQuestionActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.next)
    public void onViewClicked() {

        if (isSelected) {
            if (dataList.size() == index) {
                Toast.makeText(this, "SUBMIT", Toast.LENGTH_SHORT).show();
            } else {
                setBackgroundAll();
                setQuestions(index);
            }
        } else {
            Toast.makeText(this, "Please select any answer", Toast.LENGTH_SHORT).show();
        }

    }

    private void setBackgroundAll() {
        back1.setBackground(getDrawable(R.drawable.whitequestionboder));
        back2.setBackground(getDrawable(R.drawable.whitequestionboder));
        back3.setBackground(getDrawable(R.drawable.whitequestionboder));
        back4.setBackground(getDrawable(R.drawable.whitequestionboder));

        count1.setTextColor(getResources().getColor(R.color.white));
        count2.setTextColor(getResources().getColor(R.color.white));
        count3.setTextColor(getResources().getColor(R.color.white));
        count4.setTextColor(getResources().getColor(R.color.white));

    }

    private void setQuestions(int index) {

        question.setText(dataList.get(index).getQuestion());
        if (dataList.get(index).getChoose_option().equalsIgnoreCase("2")) {
            ans1.setText(dataList.get(index).getAnswer1());
            ans2.setText(dataList.get(index).getAnswer2());
            back3.setVisibility(View.GONE);
            back4.setVisibility(View.GONE);
        } else {
            back3.setVisibility(View.VISIBLE);
            back4.setVisibility(View.VISIBLE);
            ans1.setText(dataList.get(index).getAnswer1());
            ans2.setText(dataList.get(index).getAnswer2());
            ans3.setText(dataList.get(index).getAnswer3());
            ans4.setText(dataList.get(index).getAnswer4());
        }
        this.index++;
        isSelected = false;
    }

    @OnClick({R.id.back1, R.id.back2, R.id.back3, R.id.back4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back1:
                setBackground(back1, count1);
                break;
            case R.id.back2:
                setBackground(back2, count2);
                break;
            case R.id.back3:
                setBackground(back3, count3);
                break;
            case R.id.back4:
                setBackground(back4, count4);
                break;
        }
    }

    private void setBackground(RelativeLayout ans, TextView count) {
        isSelected = true;
        count.setTextColor(getResources().getColor(R.color.black));
        ans.setBackground(getDrawable(R.drawable.greenquestionboder));
        if (ans.getId() == R.id.back1) {
            back2.setBackground(getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getDrawable(R.drawable.whitequestionboder));

            count2.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
        } else if (ans.getId() == R.id.back2) {
            back1.setBackground(getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getDrawable(R.drawable.whitequestionboder));

            count1.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
        } else if (ans.getId() == R.id.back3) {
            back1.setBackground(getDrawable(R.drawable.whitequestionboder));
            back2.setBackground(getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getDrawable(R.drawable.whitequestionboder));
            count2.setTextColor(getResources().getColor(R.color.white));
            count1.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
        } else if (ans.getId() == R.id.back4) {
            back1.setBackground(getDrawable(R.drawable.whitequestionboder));
            back2.setBackground(getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getDrawable(R.drawable.whitequestionboder));

            count2.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count1.setTextColor(getResources().getColor(R.color.white));
        }
    }
}
