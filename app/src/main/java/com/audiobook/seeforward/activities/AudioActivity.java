package com.audiobook.seeforward.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.audiobook.seeforward.models.CheckAudioStatus;
import com.audiobook.seeforward.network.RetrofitClient;
import com.bumptech.glide.Glide;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.network.Constants;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class AudioActivity extends AppCompatActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.txtPlayingName)
    TextView txtPlayingName;
    @BindView(R.id.sBar)
    SeekBar sBar;
    @BindView(R.id.txtStartTime)
    TextView txtStartTime;
    @BindView(R.id.btnBackward)
    ImageView btnBackward;
    @BindView(R.id.btnPause)
    ImageView btnPause;
    @BindView(R.id.btnPlay)
    ImageView btnPlay;
    @BindView(R.id.btnForward)
    ImageView btnForward;
    @BindView(R.id.txtSongTime)
    TextView txtSongTime;
    @BindView(R.id.download)
    TextView download;
    private boolean audioIsPlaying = false;
    String audioFileName, audioFilePath = "", audioId,title;
    // The thread that send message to audio progress handler to update progress every one second.
    private Dialog dialog;
    private MediaPlayer mPlayer;
    private int oTime = 0, sTime = 0, eTime = 0, fTime = 5000, bTime = 5000;
    private Handler hdlr = new Handler();

    private String mExpireDate = "";
    private String mUserId = "";
    private int mCount = 0;
    private int mMaxCount = 100;
    private String mAudioHistoryId = "";
    private String mNextAudioHistoryId = "";

    private RetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        ButterKnife.bind(this);
        ProgressDailog();

        retrofitService = RetrofitClient.getInterface();
        mUserId = SharedPrefsManager.getInstance().getString(KEY_USER_ID);

        Intent intent = getIntent();
        if (intent.hasExtra("audioFilePath")) {
            title = intent.getExtras().getString("title");
            audioFilePath = intent.getExtras().getString("audioFilePath");
            audioFileName = intent.getExtras().getString("audioFileName");
            audioId = intent.getExtras().getString("id");

            if (intent.hasExtra("audio_history_id") && intent.hasExtra("audio_count")) {
                mAudioHistoryId = intent.getExtras().getString("audio_history_id");
                mCount = Integer.parseInt(intent.getExtras().getString("audio_count"));
                mNextAudioHistoryId = intent.getExtras().getString("next_audio_history_id");
                mExpireDate = intent.getExtras().getString("expire_date");
            }
            initAudioPlayer();
        }

        sBar.setClickable(false);
        btnPause.setEnabled(true);
    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        onBackPressed();
    }

    @OnClick({R.id.btnBackward, R.id.btnPause, R.id.btnPlay, R.id.btnForward, R.id.download})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnBackward:
                backwordAudio();
                break;
            case R.id.btnPause:
                pauseAudio();
               /* mPlayer.pause();
                btnPause.setEnabled(false);
                btnPlay.setEnabled(true);*/
                break;
            case R.id.btnPlay:
                startAudio();
                // playAudio();
                break;
            case R.id.btnForward:
                forwordAudio();
                break;
            case R.id.download:
                Toast.makeText(this, "Work under process...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void backwordAudio() {
        if ((sTime - bTime) > 0) {
            sTime = sTime - bTime;
            mPlayer.seekTo(sTime);
        } else {
            Toast.makeText(getApplicationContext(), "Cannot jump backward 5 seconds", Toast.LENGTH_SHORT).show();
        }
        if (!btnPlay.isEnabled()) {
            btnPlay.setEnabled(true);
        }
    }

    private void forwordAudio() {
        if ((sTime + fTime) <= eTime) {
            sTime = sTime + fTime;
            mPlayer.seekTo(sTime);
        } else {
            Toast.makeText(getApplicationContext(), "Cannot jump forward 5 seconds", Toast.LENGTH_SHORT).show();
        }
        if (!btnPlay.isEnabled()) {
            btnPlay.setEnabled(true);
        }
    }

  /*  private void playAudio() {
        mPlayer.start();
        eTime = mPlayer.getDuration();
        sTime = mPlayer.getCurrentPosition();
        if (oTime == 0) {
            sBar.setMax(eTime);
            oTime = 1;
        }
        txtSongTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(eTime),
                TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
        txtStartTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
        sBar.setProgress(sTime);
        hdlr.postDelayed(UpdateSongTime, 100);
        btnPause.setEnabled(true);
        btnPlay.setEnabled(false);
    }*/

    private Runnable UpdateSongTime = new Runnable() {
        @Override
        public void run() {
            if (mPlayer != null) {
                sTime = mPlayer.getCurrentPosition();
                txtStartTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime), TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                sBar.setProgress(sTime);
                hdlr.postDelayed(this, 100);
            }
        }
    };

    /////////////////////////////////////////////

    @Override
    public void onDestroy() {
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }

            mPlayer.release();
            mPlayer = null;
        }
        cancleDialog();
        super.onDestroy();
    }

    /* Initialize media player. */
    @SuppressLint("DefaultLocale")
    private void initAudioPlayer() {
        showProgressDialog();
        try {
            if (mPlayer == null) {
                mPlayer = new MediaPlayer();
                //String audioFilePath = audioFilePathEditor.getText().toString().trim();
                Log.d("TAG_PLAY_AUDIO", this.audioFilePath);

                if (this.audioFilePath.toLowerCase().startsWith("https://")) {
                    // Web audio from a url is stream music.
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    // Play audio from a url.
                    mPlayer.setDataSource(this.audioFilePath);
                    mPlayer.prepare();
                    eTime = mPlayer.getDuration();
                    sTime = mPlayer.getCurrentPosition();

                    if (mAudioHistoryId != null && !TextUtils.isEmpty(mAudioHistoryId)) {
                        if (mCount < mMaxCount) {
                            if (Utility.isNetAvailable(this)) {
                                postAudioCount(mUserId, audioId, mAudioHistoryId);
                            } else {
                                Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (mNextAudioHistoryId != null) {
                                if (Utility.isNetAvailable(this)) {
                                    postAudioCount(mUserId, audioId, mAudioHistoryId);
                                    unlockProgramme(mExpireDate, mNextAudioHistoryId);
                                } else {
                                    Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                                }


                                //  checkAudioStatus(mUserId, mAudioHistoryId, mNextAudioHistoryId);
                            }
                        }
                    }

                    mPlayer.setOnCompletionListener(mediaPlayer -> {
                        if (mCount < mMaxCount) {
                            mCount++;
                            mediaPlayer.seekTo(0);
                            mediaPlayer.start();

                            if (mAudioHistoryId != null && !TextUtils.isEmpty(mAudioHistoryId)) {
                                if (Utility.isNetAvailable(this)) {
                                    postAudioCount(mUserId, audioId, mAudioHistoryId);
                                } else {
                                    Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else if (mCount == mMaxCount) {
                            if (mNextAudioHistoryId != null) {
                                stopCurrentPlayAudio();
                                unlockProgramme(mExpireDate, mNextAudioHistoryId);
                                //checkAudioStatus(mUserId, mAudioHistoryId, mNextAudioHistoryId);
                            }
                            // Toast.makeText(this, "Thank you", Toast.LENGTH_SHORT).show();
                        }
                    });

                    if (oTime == 0) {
                        sBar.setMax(eTime);
                        oTime = 1;
                    }

                    Log.e("title-==",title);

                   /* //adarsh WFH
                    if (title.equals("Shaolin Breathwork Meditation")) {
                        txtPlayingName.setText("Now Playing : " + title);
                    }else {*/
                        txtPlayingName.setText("Now Playing : " + audioFileName);
                    //}///
                    txtSongTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(eTime), TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
                    txtStartTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime), TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                    sBar.setProgress(sTime);
                    startAudio();
                } else {
                    cancleDialog();
                    Toast.makeText(this, "Audio not found", Toast.LENGTH_SHORT).show();
                    Log.e("No file found", "not found");
                }
            }
        } catch (IOException ex) {
            Log.e("TAG_PLAY_AUDIO", ex.getMessage(), ex);
        }
    }

    /* Stop current play audio before play another. */
    private void stopCurrentPlayAudio() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void startAudio() {
        if (!TextUtils.isEmpty(audioFilePath) && mPlayer != null) {
            //stopCurrentPlayAudio();
            //initAudioPlayer();
            mPlayer.start();
            hdlr.postDelayed(UpdateSongTime, 100);
            audioIsPlaying = true;

            btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_play));
            btnPause.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.playicon));

            btnPlay.setEnabled(false);
            btnPause.setEnabled(true);
            cancleDialog();
        } else {
            Toast.makeText(getApplicationContext(), "Please specify an audio file to play.", Toast.LENGTH_LONG).show();
        }
    }

    public void pauseAudio() {
        if (audioIsPlaying) {
            mPlayer.pause();
            btnPlay.setEnabled(true);
            btnPause.setEnabled(false);
            //stopButton.setEnabled(true);

            btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.play));
            btnPause.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_pause));

            audioIsPlaying = false;
        }
    }

    private void ProgressDailog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(this).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showProgressDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }

    private void postAudioCount(String userId, String audioId, String audioHistoryId) {


        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ResponseBody> call = api.postAudioCount(userId, audioHistoryId, audioId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    /*PostAudioCount postAudioCount = response.body();
                    if (postAudioCount != null) {
                        if (postAudioCount.getResponse().equals("true")) {
                            //   Toast.makeText(AudioActivity.this, "Count increased", Toast.LENGTH_SHORT).show();
                        } else {
                            //   Toast.makeText(AudioActivity.this, "Error increasing count", Toast.LENGTH_SHORT).show();
                        }
                    }*/
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AudioActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.isEmpty(mAudioHistoryId)) {
            Intent intent = new Intent(AudioActivity.this, LandingScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP & Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void unlockProgramme(String expiryDate, String nextAudioHistoryId) {
        retrofitService.unlockAnotherProgram(expiryDate, nextAudioHistoryId).enqueue(new Callback<CheckAudioStatus>() {
            @Override
            public void onResponse(Call<CheckAudioStatus> call, Response<CheckAudioStatus> response) {
                if (response.isSuccessful()) {
                    CheckAudioStatus checkAudioStatus = response.body();
                    if (checkAudioStatus != null) {
                        if (checkAudioStatus.getResponse().equals("true")) {
                            showUnlockDialog();
                            //        Toast.makeText(AudioActivity.this, "You've listened this programme 100 times", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckAudioStatus> call, Throwable t) {

            }
        });
    }

    private void showUnlockDialog() {
        Dialog dialog = new Dialog(AudioActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_programme_unlocked);

        Button closeBtn = dialog.findViewById(R.id.dialog_close_btn);
        closeBtn.setOnClickListener(v -> {
            dialog.dismiss();
            finish();
        });
        /*TextView prograrammeUnlockTv = dialog.findViewById(R.id.programme_unlock_txt_view);

        prograrammeUnlockTv.setText("Congratulations, you've unlocked a new programme");*/

        dialog.show();
    }

  /*  private void checkAudioStatus(String userId, String audioHistoryId, String nextAudioHistoryId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitService api = retrofit.create(RetrofitService.class);
        Call<CheckAudioStatus> call = api.checkAudioStatus(userId, audioHistoryId, nextAudioHistoryId);
        call.enqueue(new Callback<CheckAudioStatus>() {
            @Override
            public void onResponse(Call<CheckAudioStatus> call, Response<CheckAudioStatus> response) {

                if (response.isSuccessful()) {
                    CheckAudioStatus checkAudioStatus = response.body();
                    if (checkAudioStatus != null) {
                        if (checkAudioStatus.getResponse().equals("true")) {
                            Toast.makeText(AudioActivity.this, "You've listened this programme 100 times", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckAudioStatus> call, Throwable t) {

            }
        });
    }*/
}