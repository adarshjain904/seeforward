package com.audiobook.seeforward.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.audiobook.seeforward.network.RetrofitClient;
import com.bumptech.glide.Glide;
import com.audiobook.seeforward.Adapter.QuestionListAdapter;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.AudioData;
import com.audiobook.seeforward.models.PaymentSuccess;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class QuestionsListActivity extends AppCompatActivity implements QuestionListAdapter.SelectProgram, BillingProcessor.IBillingHandler {

    QuestionListAdapter questionListAdapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rc_question)
    RecyclerView rc_question;
    private Dialog dialog;
    private String allScore = "0";
    String str_audioId, str_audio, str_title;
    List<String> audioIDArrayList = new ArrayList<>();
    //callPaymentApi(audioId, audio, title, audioIdList);
    BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_list);
        ButterKnife.bind(this);
        //put your licence key here. You can find your key in Google Play Console -> Your App Name -> Services & APIs
        bp = new BillingProcessor(this, getString(R.string.license_key), this);
        bp.initialize();
        setToolBar();
        ProgressDailog();
        rc_question = findViewById(R.id.rc_question);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rc_question.setLayoutManager(layoutManager);

        questionListAdapter = new QuestionListAdapter(this);
        rc_question.setAdapter(questionListAdapter);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                allScore = intent.getStringExtra("allScore");
                SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, allScore);

                if (Utility.isNetAvailable(QuestionsListActivity.this)) {
                    sendScoreToserver(allScore);
                } else {
                    Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

                // Log.i("rhl...S",allScore);

            } catch (Exception e) {
                e.printStackTrace();
            }
            //  String allScore = intent.getExtras().getString("allScore");
        }
    }

    private void setToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Choose Program");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendScoreToserver(String score) {
        showDialog();


        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<AudioData> call = api.postTestScore(SharedPrefsManager.getInstance().getString(KEY_USER_ID), score);
        call.enqueue(new Callback<AudioData>() {
            @Override
            public void onResponse(Call<AudioData> call, Response<AudioData> response) {
                Log.e("response", response.body().toString());
                cancleDialog();
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AudioData audioData = response.body();
                    // AudioData.UserDeatailsData userDeatailsData = audioData.getUser_details();
                    List<AudioData.AudioDataList> audioDataLists = audioData.getAudioDataLists();

                    if (audioDataLists != null && audioDataLists.size() > 0) {
                        questionListAdapter.addCustomerList(audioDataLists);
                        questionListAdapter.setSelectProgram(QuestionsListActivity.this);
                        Log.i("rhl...", "yes");
                    }
                } else {
                    Toast.makeText(QuestionsListActivity.this, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AudioData> call, Throwable t) {
                Toast.makeText(QuestionsListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                cancleDialog();
            }
        });
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        finish();
    }

    private void ProgressDailog() {
        dialog = new Dialog(QuestionsListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(QuestionsListActivity.this).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }

    @Override
    public void chooseProgram(String id, String audio, String title, List<String> audioIdList) {
        showPaymentDialog(id, audio, title, audioIdList);
    }

    public void showPaymentDialog(String audioId, String audio, String title, List<String> audioIdList) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", audioId);
        Button pay = dialog.findViewById(R.id.pay);
        pay.setOnClickListener(v -> {
                    dialog.dismiss();

                    if (Utility.isNetAvailable(QuestionsListActivity.this)) {
                        str_audioId = audioId;
                        str_audio = audio;
                        str_title = title;
                        audioIDArrayList = audioIdList;
                        //change your product id here

                        //  callPaymentApi(str_audioId, str_audio, str_title, audioIDArrayList, "XYZ");
                        bp.purchase(QuestionsListActivity.this, "mental.health_strength1_4");

                    } else {
                        Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                    // showCongratsDialog(id,audio,title);
                }
        );

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> dialog.dismiss()
        );
        dialog.show();
    }

    public void showCongratsDialog(String id, String audio, String title) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.congrats_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.e("audio id", id);
        Button cancel = dialog.findViewById(R.id.okay);
        cancel.setOnClickListener(v -> {
                    dialog.dismiss();
                    Intent intent = new Intent(QuestionsListActivity.this, AudioActivity.class);
                    intent.putExtra("audioFilePath", audio);
                    intent.putExtra("audioFileName", title);
                    intent.putExtra("id", id);
                    startActivity(intent);
                    finish();
                }
        );
        dialog.show();
    }

    private void callPaymentApi(String audioId, String audio, String title, List<String> audioList, String g_order_id) {
        StringBuilder otherAudioId = new StringBuilder();
        for (int i = 0; i < audioList.size(); i++) {
            String id = audioList.get(i);
            if (!audioId.equals(id)) {
                otherAudioId.append(id);
                if (i < audioList.size() - 1) {
                    otherAudioId.append(",");
                }
            }
        }

        showDialog();



        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<PaymentSuccess> call = api.paymentApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID),
                "Programme", "90", "100", g_order_id, audioId, allScore, otherAudioId.toString().trim());
        call.enqueue(new Callback<PaymentSuccess>() {
            @Override
            public void onResponse(Call<PaymentSuccess> call, Response<PaymentSuccess> response) {
                Log.e("response", response.body().toString());
                cancleDialog();
                if (response.body() != null) {
                    PaymentSuccess paymentSuccess = response.body();
                    if (paymentSuccess.getResponse().equalsIgnoreCase("true")) {
                        SharedPrefsManager.getInstance().removeKey(Utility.PREF_SCORE);
                        showCongratsDialog(audioId, audio, title);
                    } else {
                        Toast.makeText(QuestionsListActivity.this, "Payment failed", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(QuestionsListActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentSuccess> call, Throwable t) {
                Toast.makeText(QuestionsListActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                cancleDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(QuestionsListActivity.this, LandingScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        //if Multiple products
        /* switch (productId){
            case "buy_3_boost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"3");
                break;
            case "buy_10_bbost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"10");
                break;
            case "buy_20_boost":
                purchaseBoost("4",details.purchaseInfo.purchaseData.orderId,"20");
                break;
        }*/

        callPaymentApi(str_audioId, str_audio, str_title, audioIDArrayList, details.purchaseInfo.purchaseData.orderId);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
}
