package com.audiobook.seeforward.views;

import android.content.Context;
import android.util.DisplayMetrics;
import android.widget.ExpandableListView;

public class CustomExpListView extends ExpandableListView {
    private Context context;

    public CustomExpListView(Context context) {
        super(context);
        this.context = context;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(20000, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}

