package com.audiobook.seeforward.utiles;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.audiobook.seeforward.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Utility {
    public static final String SLIDEMENURBROADCAST = "MyProjectSlideMenu";
    private static String PREFERENCES = "MyProjectPreference";
    public  static final String CURRENCYRATE ="currency_rate";
    public static final String CURRENCYCODE="currency_code";

    public static final String PREF_SCORE = "pref_score_value";
    public static final String PREF_DAY_DATA = "pref_data_score";

   // public static final String GENDER="Gender";
    public static Dialog dialog;

    public static boolean isNetAvailable(Context context){

        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else
            connected = false;

        return connected;
       /* ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()){
                return true;
            }
        }else {
            return false;
        }
        return false;*/
    }


    public static void showDeductionDialog(final Context context) {
        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.info_dailog);

        LinearLayout btnCancel = dialog.findViewById(R.id.okay);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "click Dialog", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public static void logLargeString(String str) {

        try {
            if (str.length() > 3000) {
                System.out.print(str.substring(0, 3000));
                logLargeString(str.substring(3000));
            } else {
                System.out.print(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDeviceType(Context mContext) {

        String ua = new WebView(mContext).getSettings().getUserAgentString();
        if (ua.contains("Mobile")) {
            System.out.println("Type:Mobile");
            return "ANDROID MOBILE";
            // Your code for Mobile
        } else {
            // Your code for TAB
            System.out.println("Type:TAB");
            return "ANDROID TAB";
        }
    }

    public static Boolean write(String fname, String fcontent) {
        try {

            String fpath = "/sdcard/" + fname + ".txt";
            File file = new File(fpath);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();

            Log.d("Suceess", "Sucess");
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getFormatedDate(String strDate, String sourceFormate, String destinyFormate) {
        SimpleDateFormat df;
        df = new SimpleDateFormat(sourceFormate);
        Date date = null;
        try {
            date = df.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        df = new SimpleDateFormat(destinyFormate);
        return df.format(date);

    }

    public static void setBooleanPreferences(Context context, String key, boolean isCheck) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.putBoolean(key, isCheck);
        editor.commit();
    }

    public static boolean getBooleanPreferences(Context context, String key) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        return setting.getBoolean(key, false);
    }

    public static void setStringPreferences(Context context, String key, String value) {
        try {
            SharedPreferences setting = context.getSharedPreferences(PREFERENCES, 0);
            SharedPreferences.Editor editor = setting.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getStringPreferences(Context context, String key) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        return setting.getString(key, null);
    }

    public static void removeStringPreferences(Context context, String key) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.remove(key);
        editor.commit();

    }

    public static void setIntegerPreferences(Context context, String key, int value) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static void clearAllSharedPreferences(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }
    public static void clearwelcomePreferences(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.remove("checkrefral");
        editor.commit();
    }
    public static int getIntegerPreferences(Context context, String key) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        return setting.getInt(key, 0);

    }
    public static long getLongprefrences(Context context, String key) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        return setting.getLong(key, 0);

    }

    public static void setLongPreferences(Context context, String key, long value) {
        SharedPreferences setting = (SharedPreferences) context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.putLong(key, value);
        editor.commit();

    }


    public static void showAlert(Context mContext, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(msg);
        // Set behavior of negative button

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub

            }
        });

        AlertDialog alert = builder.create();
        try {
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressHUD(Context context, String title,
                                       String message) {
        try {

            if (title == null)
                title = "";
            if (message == null)
                message = "";
            dialog = ProgressDialog.show(context, title, message);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public static void hideProgressHud() {
        if (dialog != null)
            dialog.cancel();
    }
    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for snackbar
     */

    public static Snackbar setSnake(Context context, String msg, View view){
        Snackbar snackbar = Snackbar.make(view, ""+msg, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        //   snackBarView.setBackgroundColor(context.getResources().getColor(R.color.text_color));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        // textView.setTextColor(context.getResources().getColor(R.color.white));
        snackbar.show();
        return snackbar;
    }


}
