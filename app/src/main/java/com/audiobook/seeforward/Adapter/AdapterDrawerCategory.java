package com.audiobook.seeforward.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.Audio;
import com.audiobook.seeforward.models.ProgrammWithAudioList;

import java.util.ArrayList;

public class AdapterDrawerCategory extends BaseExpandableListAdapter {

    private final Context context;
    private final ProgrammWithAudioList listDataHeader;
    ArrayList<Audio> audioList;
    private int parentGroupPosition;

    public AdapterDrawerCategory(Context context, ProgrammWithAudioList listDataHeader, int parentGroupPosition) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        audioList = listDataHeader == null ? null : listDataHeader.getAudiolist();
        this.parentGroupPosition = parentGroupPosition;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return audioList == null ? 0 : audioList.size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return listDataHeader == null ? null : listDataHeader.getSubHaading();
    }

    @Override
    public ArrayList<Audio> getChild(int groupPosition, int childPosition) {
        return audioList;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_drawer_sub_items, null);
        }
        LinearLayout container = convertView.findViewById(R.id.tv_item_main_category_with_image);
        /*LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        container.setLayoutParams(lp);*/

        TextView tvSubHeading = convertView.findViewById(R.id.tv_program_desc);
        if (listDataHeader.getPurchaseButton()) {
            tvSubHeading.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        } else {
            tvSubHeading.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, context.getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp), null);
        }
        if (TextUtils.isEmpty(listDataHeader.getSubHaading())) {
            tvSubHeading.setVisibility(View.GONE);
        } else {
            tvSubHeading.setVisibility(View.VISIBLE);
        }
        tvSubHeading.setText(listDataHeader == null ? "" : listDataHeader.getSubHaading());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.quetion_audio_list, null);
        }
        LinearLayout container = convertView.findViewById(R.id.layout_item);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        container.setLayoutParams(lp);
        TextView AudioTitle = convertView.findViewById(R.id.tv_title);
        AudioTitle.setText(audioList == null ? null : audioList.get(childPosition).getAudioTitle());
        FrameLayout frameLayout = convertView.findViewById(R.id.fl_to_hide);
        if (audioList.get(childPosition).getAudioEnabled()) {
            frameLayout.setVisibility(View.GONE);
        } else {
            frameLayout.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
