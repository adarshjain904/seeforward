package com.audiobook.seeforward.Adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.AudioActivity;
import com.audiobook.seeforward.models.AudioData;

import java.util.List;


public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.CustomerViewHolder> {

    private FragmentActivity context;
    private List<AudioData.AudioDataList> audioDataLists;

    public HistoryListAdapter(FragmentActivity context) {
        this.context = context;
    }


    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.quetion_audio_list, parent, false);

        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerViewHolder holder, int position) {
        final AudioData.AudioDataList audioDataList = audioDataLists.get(position);

        holder.tv_title.setText(audioDataList.getTitle());
        holder.tv_level.setText(audioDataList.getLevel());

        Log.i("rhl....!!",audioDataList.getTitle());

                   /* Intent intent=new Intent(QuestionsListActivity.this, AudioActivity.class);
                    intent.putExtra("audioFilePath",data.getAudiofile());
                    intent.putExtra("audioFileName",data.getAudiotitle());
                    startActivity(intent);*/

        holder.layout_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AudioActivity.class);
                intent.putExtra("audioFilePath", audioDataList.getAudio());
                intent.putExtra("audioFileName", audioDataList.getTitle());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return null == audioDataLists ? 0 : audioDataLists.size();
    }

    public void addCustomerList(List<AudioData.AudioDataList> homeListList) {
        this.audioDataLists = homeListList;
        notifyDataSetChanged();
    }

    class CustomerViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_level;
        private LinearLayout layout_item;

        CustomerViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_level = itemView.findViewById(R.id.tv_level);
            layout_item = itemView.findViewById(R.id.layout_item);

        }
    }

}