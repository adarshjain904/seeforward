package com.audiobook.seeforward.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.AudioData;

import java.util.ArrayList;
import java.util.List;


public class QuestionListAdapter extends RecyclerView.Adapter<QuestionListAdapter.CustomerViewHolder> {

    private Context context;
    private List<AudioData.AudioDataList> audioDataLists;
    public SelectProgram selectProgram;

    public SelectProgram getSelectProgram() {
        return selectProgram;
    }

    public void setSelectProgram(SelectProgram selectProgram) {
        this.selectProgram = selectProgram;
    }

    public interface SelectProgram {
        void chooseProgram(String id, String audio, String title, List<String> audioIdList);
    }

    public QuestionListAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.quetion_audio_list, parent, false);
        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerViewHolder holder, int position) {
        final AudioData.AudioDataList audioDataList = audioDataLists.get(position);

        if (position == 0){
            holder.itemView.setVisibility(View.VISIBLE);
        }
        else {
            holder.itemView.setVisibility(View.GONE);
        }

        holder.tv_title.setText(audioDataList.getTitle());
        holder.tv_level.setText(audioDataList.getLevel());

                   /* Intent intent=new Intent(QuestionsListActivity.this, AudioActivity.class);
                    intent.putExtra("audioFilePath",data.getAudiofile());
                    intent.putExtra("audioFileName",data.getAudiotitle());
                    startActivity(intent);*/

        holder.layout_item.setOnClickListener(view -> {
            if (position == 0){
                List<String> audioIdList = new ArrayList<>();
                for (int i=0; i< audioDataLists.size(); i++){
                    if (i != position){
                        audioIdList.add(audioDataLists.get(i).getId());
                    }
                }
                selectProgram.chooseProgram(audioDataList.getId(), audioDataList.getAudio(), audioDataList.getTitle(), audioIdList);
            }
            else {
                Toast.makeText(context, "Please purchase the first program", Toast.LENGTH_SHORT).show();
            }

           /* Intent intent = new Intent(context, AudioActivity.class);
            intent.putExtra("audioFilePath", audioDataList.getAudio());
            intent.putExtra("audioFileName", audioDataList.getTitle());
            context.startActivity(intent);*/
        });
    }

    @Override
    public int getItemCount() {
        return null == audioDataLists ? 0 : audioDataLists.size();
    }

    public void addCustomerList(List<AudioData.AudioDataList> homeListList) {
        this.audioDataLists = homeListList;
        notifyDataSetChanged();
    }

    class CustomerViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_level;
        private LinearLayout layout_item;

        CustomerViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_level = itemView.findViewById(R.id.tv_level);
            layout_item = itemView.findViewById(R.id.layout_item);

        }
    }

}