package com.audiobook.seeforward.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.AudioActivity;
import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.activities.MyProgrammesActivity;
import com.audiobook.seeforward.models.Audio;
import com.audiobook.seeforward.models.ProgrammWithAudioList;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import java.util.ArrayList;

public class AdapterDrawerItem extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<ProgrammWithAudioList> drawerItemList;

    public AdapterDrawerItem(Context context, ArrayList<ProgrammWithAudioList> drawerItemList) {
        this.context = context;
        this.drawerItemList = drawerItemList;
    }

    @Override
    public int getGroupCount() {
        return drawerItemList == null ? 0 : drawerItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return drawerItemList.get(groupPosition).getAudiolist() == null ? 0 : drawerItemList.get(groupPosition).getAudiolist().size();
    }

    /*@Override
    public Object getGroup(int i) {
        return drawerItemList.get(i);
    }*/

    @Override
    public ProgrammWithAudioList getGroup(int groupPosition) {
        return drawerItemList.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        if (drawerItemList.get(groupPosition).getAudiolist() != null && drawerItemList.get(groupPosition).getAudiolist().size() > 0) {
            return drawerItemList.get(groupPosition).getSubHaading();
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        //  if (convertView == null) {


        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.item_drawer_items, null);
        TextView tvHeading = convertView.findViewById(R.id.tv_program_heading);
        TextView tv_program_desc = convertView.findViewById(R.id.tv_program_desc);
        TextView buyNow = convertView.findViewById(R.id.tv_program_buy_now);

        tvHeading.setText(drawerItemList.get(groupPosition).getHeading());
        tv_program_desc.setText(drawerItemList.get(groupPosition) == null ? "" : drawerItemList.get(groupPosition).getSubHaading());
        if (drawerItemList.get(groupPosition).getPurchaseButton()) {
            buyNow.setVisibility(View.VISIBLE);
            if (drawerItemList.get(groupPosition).getAmount().equals("0")){
                buyNow.setText("Free access");
            }
        } else {
            buyNow.setVisibility(View.GONE);
        }

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mAllScore = SharedPrefsManager.getInstance().getString(Utility.PREF_SCORE);
                if (context instanceof MyProgrammesActivity) {
                    if (drawerItemList.get(groupPosition).getAmount().equals("0")){
                        ((MyProgrammesActivity) context).showSubscribeDialog(drawerItemList.get(groupPosition).getPlanId());
                    } else
                        ((MyProgrammesActivity) context).showPaymentDialog(drawerItemList.get(groupPosition).getPlanId(), drawerItemList.get(groupPosition).getAmount(), drawerItemList.get(groupPosition).getAudiolist());

                } else if (context instanceof LandingScreenActivity)
                    ((LandingScreenActivity) context).showPaymentDialog(drawerItemList.get(groupPosition).getPlanId(), drawerItemList.get(groupPosition).getAmount(), drawerItemList.get(groupPosition).getAudiolist());

                    /*Intent intent = new Intent(context, BuyProgramActivity.class);
                    intent.putExtra("allScore", mAllScore);
                    intent.putExtra("plan_id", drawerItemList.get(groupPosition).getPlanId());
                    context.startActivity(intent);*/
            }
        });

        if (isExpanded) {
            tv_program_desc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up_black_24dp, 0);

        } else {
            tv_program_desc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);

        }
        // }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // if (convertView == null) {
        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.quetion_audio_list, null);
        //  }
      /*  LinearLayout container = convertView.findViewById(R.id.layout_item);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        container.setLayoutParams(lp);*/
        TextView AudioTitle = convertView.findViewById(R.id.tv_title);
        LinearLayout layoutItem = convertView.findViewById(R.id.layout_item);
        AudioTitle.setText(drawerItemList.get(groupPosition).getAudiolist() == null ? null : drawerItemList.get(groupPosition).getAudiolist().get(childPosition).getAudioTitle());
        FrameLayout frameLayout = convertView.findViewById(R.id.fl_to_hide);
        if (drawerItemList.get(groupPosition).getAudiolist().get(childPosition).getAudioEnabled()) {
            frameLayout.setVisibility(View.GONE);
        } else {
            frameLayout.setVisibility(View.VISIBLE);
        }

        layoutItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Audio audio = drawerItemList.get(groupPosition).getAudiolist().get(childPosition);
                if (audio.getAudioEnabled()) {
                    Intent intent = new Intent(context, AudioActivity.class);
                    intent.putExtra("title", drawerItemList.get(groupPosition).getHeading());
                    intent.putExtra("audioFilePath", audio.getAudiolink());
                    intent.putExtra("audioFileName", audio.getAudioTitle());
                    intent.putExtra("id", audio.getAudioId());
                    intent.putExtra("audio_history_id", audio.getAudioHistoryId());
                    intent.putExtra("audio_count", audio.getListenCount());
                    intent.putExtra("expire_date", audio.getPlanExpiry());
                    context.startActivity(intent);
                }
            }
        });


        return convertView;


       /*
        ProgrammWithAudioList list = drawerItemList.get(groupPosition);

        final CustomExpListView secondLevelExpListView = new CustomExpListView(context);
        secondLevelExpListView.setAdapter(new AdapterDrawerCategory(context, list, groupPosition));
        secondLevelExpListView.setGroupIndicator(null);
        secondLevelExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Audio audio = list.getAudiolist().get(childPosition);
                if (audio.getAudioEnabled()) {
                    Intent intent = new Intent(context, AudioActivity.class);
                    intent.putExtra("audioFilePath", audio.getAudiolink());
                    intent.putExtra("audioFileName", audio.getAudioTitle());
                    intent.putExtra("id", audio.getAudioId());
                    intent.putExtra("audio_history_id", audio.getAudioHistoryId());
                    intent.putExtra("audio_count", audio.getListenCount());
                    intent.putExtra("expire_date", audio.getPlanExpiry());
                    context.startActivity(intent);
                }

                return false;
            }
        });
        secondLevelExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    secondLevelExpListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        return secondLevelExpListView;*/
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
