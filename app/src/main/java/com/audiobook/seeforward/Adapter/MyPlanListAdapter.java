package com.audiobook.seeforward.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.AudioActivity;
import com.audiobook.seeforward.models.MyPlans;

import java.util.List;

public class MyPlanListAdapter extends RecyclerView.Adapter<MyPlanListAdapter.CustomerViewHolder> {

    private FragmentActivity context;
    private List<MyPlans.Data> audioDataLists;
    private boolean isClicked = false;

    public MyPlanListAdapter(FragmentActivity context) {
        this.context = context;
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.quetion_audio_list, parent, false);
        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerViewHolder holder, int position) {
        final MyPlans.Data audioDataList = audioDataLists.get(position);

        String audioId = audioDataList.getAudio_id();
        String audioCount = audioDataList.getListen_count();
        String audioHistoryId = audioDataList.getAudio_history_id();
        String expireDate = audioDataList.getExp_date();

        String status = audioDataList.getStatus();

        holder.tv_title.setText(audioDataList.getTitle());
        //holder.tv_level.setText(audioDataList.getLevel());

        if (status.equals("1")) {
            holder.tv_title.setVisibility(View.VISIBLE);
            holder.listCardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.tv_title.setVisibility(View.INVISIBLE);
            holder.listCardView.setCardBackgroundColor(context.getResources().getColor(R.color.gray));
        }

        if (Integer.parseInt(audioCount) < 100) {
        }

        holder.layout_item.setOnClickListener(view -> {
            if (status.equals("1")) {
                if (!isClicked) {
                    isClicked = true;
                    Intent intent = new Intent(context, AudioActivity.class);
                    intent.putExtra("audioFilePath", audioDataList.getAudio());
                    intent.putExtra("audioFileName", audioDataList.getTitle());
                    intent.putExtra("id", audioId);
                    intent.putExtra("audio_history_id", audioHistoryId);
                    intent.putExtra("audio_count", audioCount);
                    intent.putExtra("expire_date", expireDate);
                    try {
                        intent.putExtra("next_audio_history_id", audioDataLists.get(position + 1).getAudio_history_id());
                    } catch (Exception e) {
                    }
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == audioDataLists ? 0 : audioDataLists.size();
    }

    public void addCustomerList(List<MyPlans.Data> homeListList) {
        this.audioDataLists = homeListList;
        notifyDataSetChanged();
    }

    public void makeClickFalse(boolean isClicked){
        this.isClicked = isClicked;
    }

    class CustomerViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_level;
        private CardView listCardView;
        private LinearLayout layout_item;

        CustomerViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_level = itemView.findViewById(R.id.tv_level);
            layout_item = itemView.findViewById(R.id.layout_item);
            listCardView = itemView.findViewById(R.id.audio_list_card_view);
        }
    }
}