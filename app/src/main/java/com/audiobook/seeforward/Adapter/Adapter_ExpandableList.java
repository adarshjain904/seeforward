package com.audiobook.seeforward.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.ExpandableModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 24-01-2018.
 */

public class Adapter_ExpandableList extends BaseExpandableListAdapter {

    Context mContext;
    ArrayList<ExpandableModel> _list;
    LayoutInflater layoutInflater;

    public Adapter_ExpandableList(Context mContext, ArrayList<ExpandableModel> _list) {
        this.mContext = mContext;
        this._list = _list;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    //GET A SINGLE MENU NAME
    @Override
    public Object getChild(int groupPos, int childPos) {
        // TODO Auto-generated method stub
        return _list.get(groupPos).menu_list.get(childPos);
    }

    //GET MENU ID
    @Override
    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }


    //GET MENU ROW
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {

        //ONLY INFLATER XML ROW LAYOUT IF ITS NOT PRESENT,OTHERWISE REUSE IT

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_submenu, null);
        }
        //GET CHILD/MENUS NAME
        String child = (String) getChild(groupPosition, childPosition);

        //SET CHILD NAME
        //TextView txtListChild = (TextView) convertView.findViewById(R.id.submenu);

        TextView menus_txt = (TextView) convertView.findViewById(R.id.submenu);
        menus_txt.setText(child);
        // menus_txt.setTypeface(Utilities.myFont(mContext));

        //GET MENU NAME
        String menu_name = getGroup(groupPosition).toString();

        return convertView;
    }

    //GEt NUMBER OF MENUS
    @Override
    public int getChildrenCount(int groupPosition) {
        return _list.get(groupPosition).menu_list.size();
    }

    //GET MENUS
    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return _list.get(groupPosition);
    }

    //GET NUMBER OF MAIN_MENUS
    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return _list.size();
    }

    //GET MAIN_MENUS ID
    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    //GET TEAM ROW
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //ONLY INFLATE XML TEAM ROW MODEL IF ITS NOT PRESENT,OTHERWISE REUSE IT
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listheader, null);
        }
        //GET GROUP/MAIN_MENU ITEM
        ExpandableModel t = (ExpandableModel) getGroup(groupPosition);
        ExpandableModel t_ = (ExpandableModel) getGroup(groupPosition);

        //SET GROUP NAME
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.submenu);
        ImageView headerIcon = (ImageView) convertView.findViewById(R.id.iconimage);
        ImageView indicator_img = (ImageView) convertView.findViewById(R.id.exp_indication);
        String name = t.Name;
        lblListHeader.setText(name);
        // nameTv.setTypeface(Utilities.myFont(mContext));
        int imgsrc = t_.Image;
        headerIcon.setImageResource(imgsrc);

        if (getChildrenCount(groupPosition)> 0) {
            indicator_img.setVisibility(View.VISIBLE);
            indicator_img.setImageResource(isExpanded ? R.drawable.ic_keyboard_arrow_up_black_24dp : R.drawable.ic_keyboard_arrow_down_black_24dp);
        } else {
            indicator_img.setVisibility(View.INVISIBLE);

        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}