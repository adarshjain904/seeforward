package com.audiobook.seeforward.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProgrammWithAudioList implements Parcelable {
    @SerializedName("heading")
    private String heading;
    @SerializedName("sub_haading")
    private String subHaading;
    @SerializedName("plan_id")
    private String planId;
    @SerializedName("amount")
    private String Amount;
    @SerializedName("purchase_button")
    private Boolean purchaseButton;
    @SerializedName("audiolist")
    private ArrayList<Audio> audiolist = null;

    public ProgrammWithAudioList() {
    }

    protected ProgrammWithAudioList(Parcel in) {
        heading = in.readString();
        subHaading = in.readString();
        planId = in.readString();
        Amount = in.readString();
        byte tmpPurchaseButton = in.readByte();
        purchaseButton = tmpPurchaseButton == 0 ? null : tmpPurchaseButton == 1;
        audiolist = in.createTypedArrayList(Audio.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(heading);
        dest.writeString(subHaading);
        dest.writeString(planId);
        dest.writeString(Amount);
        dest.writeByte((byte) (purchaseButton == null ? 0 : purchaseButton ? 1 : 2));
        dest.writeTypedList(audiolist);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProgrammWithAudioList> CREATOR = new Creator<ProgrammWithAudioList>() {
        @Override
        public ProgrammWithAudioList createFromParcel(Parcel in) {
            return new ProgrammWithAudioList(in);
        }

        @Override
        public ProgrammWithAudioList[] newArray(int size) {
            return new ProgrammWithAudioList[size];
        }
    };

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public ProgrammWithAudioList withHeading(String heading) {
        this.heading = heading;
        return this;
    }

    public String getSubHaading() {
        return subHaading;
    }

    public void setSubHaading(String subHaading) {
        this.subHaading = subHaading;
    }

    public ProgrammWithAudioList withSubHaading(String subHaading) {
        this.subHaading = subHaading;
        return this;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public ProgrammWithAudioList withPlanId(String planId) {
        this.planId = planId;
        return this;
    }

    public Boolean getPurchaseButton() {
        return purchaseButton;
    }

    public void setPurchaseButton(Boolean purchaseButton) {
        this.purchaseButton = purchaseButton;
    }

    public ProgrammWithAudioList withPurchaseButton(Boolean purchaseButton) {
        this.purchaseButton = purchaseButton;
        return this;
    }

    public ArrayList<Audio> getAudiolist() {
        return audiolist;
    }

    public void setAudiolist(ArrayList<Audio> audiolist) {
        this.audiolist = audiolist;
    }

    public ProgrammWithAudioList withAudiolist(ArrayList<Audio> audiolist) {
        this.audiolist = audiolist;
        return this;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
