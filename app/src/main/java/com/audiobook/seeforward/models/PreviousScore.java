package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreviousScore {
    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("data")
    @Expose
    private PreviousScoreData previousScoreData;

    public void setResponse(String response) {
        this.response = response;
    }

    public void setPreviousScoreData(PreviousScoreData previousScoreData) {
        this.previousScoreData = previousScoreData;
    }

    public String getResponse() {
        return response;
    }

    public PreviousScoreData getPreviousScoreData() {
        return previousScoreData;
    }

    public static class PreviousScoreData{
        @SerializedName("total_score")
        @Expose
        private String totalScore;

        public void setTotalScore(String totalScore) {
            this.totalScore = totalScore;
        }

        public String getTotalScore() {
            return totalScore;
        }
    }
}
