package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AudioData {

    @Expose
    @SerializedName("response")
    private String response;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("user_details")
    private UserDeatailsData user_details;

    @Expose
    @SerializedName("audio_list")
    private List<AudioDataList> audioDataLists;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDeatailsData getUser_details() {
        return user_details;
    }

    public void setUser_details(UserDeatailsData user_details) {
        this.user_details = user_details;
    }

    public List<AudioDataList> getAudioDataLists() {
        return audioDataLists;
    }

    public void setAudioDataLists(List<AudioDataList> audioDataLists) {
        this.audioDataLists = audioDataLists;
    }

    public static class UserDeatailsData {
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("user_name")
        private String user_name;
        @Expose
        @SerializedName("user_email")
        private String user_email;
        @Expose
        @SerializedName("user_mobile")
        private String user_mobile;
        @Expose
        @SerializedName("user_img")
        private String user_img;
        @Expose
        @SerializedName("user_status")
        private String user_status;
        @Expose
        @SerializedName("created_date")
        private String created_date;
        @Expose
        @SerializedName("modified")
        private String modified;
        @Expose
        @SerializedName("gender")
        private String gender;
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("score")
        private String score;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_email() {
            return user_email;
        }

        public void setUser_email(String user_email) {
            this.user_email = user_email;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getUser_img() {
            return user_img;
        }

        public void setUser_img(String user_img) {
            this.user_img = user_img;
        }

        public String getUser_status() {
            return user_status;
        }

        public void setUser_status(String user_status) {
            this.user_status = user_status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }
    }

    public static class AudioDataList {

        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("level")
        private String level;
        @Expose
        @SerializedName("audio")
        private String audio;
        @Expose
        @SerializedName("points")
        private String points;
        @Expose
        @SerializedName("title")
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

}
       /*"response": true,
    "message": "Data Found",
    "user_details": {
        "user_id": "1",
        "user_name": "ssssss",
        "user_email": "test@gmail.com",
        "user_mobile": "12345555",
        "user_pass": "12345",
        "user_img": "upload/admin/User/scandemo.jpg",
        "user_status": "1",
        "created_date": "2019-09-13 15:45:50",
        "modified": "2019-09-13 15:45:50",
        "gender": "Female",
        "dob": "01/08/1990",
        "score": "1254"
    },
    "audio_list": [
        {
            "id": "4",
            "level": "2",
            "audio": "upload/admin/audio/SampleAudio_0_4mb52.mp3",
            "points": "",
            "title": "Better state of mind"
        }
    ]*/
