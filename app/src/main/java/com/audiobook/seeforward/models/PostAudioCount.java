package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAudioCount {
    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("message")
    @Expose
    private String message;

    public void setResponse(String response) {
        this.response = response;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }
}
