package com.audiobook.seeforward.models;

import java.util.ArrayList;

/**
 * Created by Administrator on 24-01-2018.
 */

public class ExpandableModel {
  public String Name;
  public int Image;
  public int ArrowImage;

  public ArrayList<String> menu_list = new ArrayList<String>();



  public ExpandableModel(String name, int image, ArrayList<String> menu_list) {
    Name = name;
    Image = image;

    this.menu_list = menu_list;
  }


  public ExpandableModel(String name, int image) {
    Name = name;
    Image = image;
  }

  /*// public Nav_Menus(String name) {
       Name = name;
   }
*/
  @Override
  public String toString() {
    return Name;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public int getImage() {
    return Image;
  }

  public void setImage(int image) {
    Image = image;
  }

  public int getArrowImage() {
    return ArrowImage;
  }

  public void setArrowImage(int arrowImage) {
    ArrowImage = arrowImage;
  }

  public ArrayList<String> getMenu_list() {
    return menu_list;
  }

  public void setMenu_list(ArrayList<String> menu_list) {
    this.menu_list = menu_list;
  }
}
