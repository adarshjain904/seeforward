package com.audiobook.seeforward.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyProfileModel {
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private MyProfileListModel data;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MyProfileListModel getData() {
        return data;
    }

    public void setData(MyProfileListModel data) {
        this.data = data;
    }

    public class MyProfileListModel implements Parcelable {

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("user_email")
        @Expose
        private String userEmail;
        @SerializedName("user_mobile")
        @Expose
        private String userMobile;
        @SerializedName("user_pass")
        @Expose
        private String userPass;
        @SerializedName("user_img")
        @Expose
        private String userImg;
        @SerializedName("user_status")
        @Expose
        private String userStatus;
        @SerializedName("created_date")
        @Expose
        private String createdDate;
        @SerializedName("modified")
        @Expose
        private String modified;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("score")
        @Expose
        private String score;
        @SerializedName("login_type")
        @Expose
        private String loginType;
        @SerializedName("social_id")
        @Expose
        private String socialId;
         @SerializedName("user_company")
        @Expose
        private String userCompany;
         @SerializedName("user_department")
        @Expose
        private String userDepartment;
         @SerializedName("user_role")
        @Expose
        private String userRole;
         @SerializedName("user_branch")
        @Expose
        private String userBranch;
         @SerializedName("user_location")
        @Expose
        private String userLocation;
         @SerializedName("user_region")
        @Expose
        private String userRegion;
         @SerializedName("emp_name_for_home")
        @Expose
        private String empNameForHome;
         @SerializedName("full_name")
        @Expose
        private String fullName;
         @SerializedName("user_country")
        @Expose
        private String userCountry;


        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public String getUserCompany() {
            return userCompany;
        }

        public void setUserCompany(String userCompany) {
            this.userCompany = userCompany;
        }

        public String getUserDepartment() {
            return userDepartment;
        }

        public void setUserDepartment(String userDepartment) {
            this.userDepartment = userDepartment;
        }

        public String getUserRole() {
            return userRole;
        }

        public void setUserRole(String userRole) {
            this.userRole = userRole;
        }

        public String getUserBranch() {
            return userBranch;
        }

        public void setUserBranch(String userBranch) {
            this.userBranch = userBranch;
        }

        public String getUserLocation() {
            return userLocation;
        }

        public void setUserLocation(String userLocation) {
            this.userLocation = userLocation;
        }

        public String getUserRegion() {
            return userRegion;
        }

        public void setUserRegion(String userRegion) {
            this.userRegion = userRegion;
        }

        public String getEmpNameForHome() {
            return empNameForHome;
        }

        public void setEmpNameForHome(String empNameForHome) {
            this.empNameForHome = empNameForHome;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getUserCountry() {
            return userCountry;
        }

        public void setUserCountry(String userCountry) {
            this.userCountry = userCountry;
        }



        public MyProfileListModel() {
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }



        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }



        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }


        public String getUserMobile() {
            return userMobile;
        }

        public void setUserMobile(String userMobile) {
            this.userMobile = userMobile;
        }



        public String getUserPass() {
            return userPass;
        }

        public void setUserPass(String userPass) {
            this.userPass = userPass;
        }



        public String getUserImg() {
            return userImg;
        }

        public void setUserImg(String userImg) {
            this.userImg = userImg;
        }


        public String getUserStatus() {
            return userStatus;
        }

        public void setUserStatus(String userStatus) {
            this.userStatus = userStatus;
        }



        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }



        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }



        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }



        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }



        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }



        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }



       /* public final Parcelable.Creator<MyProfileModel.MyProfileListModel> CREATOR = new Creator<MyProfileListModel>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public MyProfileListModel createFromParcel(Parcel in) {
                return new MyProfileListModel(in);
            }

            public MyProfileListModel[] newArray(int size) {
                return (new MyProfileListModel[size]);
            }

        };
        protected MyProfileListModel(Parcel in) {
            this.userId = ((String) in.readValue((String.class.getClassLoader())));
            this.userName = ((String) in.readValue((String.class.getClassLoader())));
            this.userEmail = ((String) in.readValue((String.class.getClassLoader())));
            this.userMobile = ((String) in.readValue((String.class.getClassLoader())));
            this.userPass = ((String) in.readValue((String.class.getClassLoader())));
            this.userImg = ((String) in.readValue((String.class.getClassLoader())));
            this.userStatus = ((String) in.readValue((String.class.getClassLoader())));
            this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
            this.modified = ((String) in.readValue((String.class.getClassLoader())));
            this.gender = ((String) in.readValue((String.class.getClassLoader())));
            this.dob = ((String) in.readValue((String.class.getClassLoader())));
            this.score = ((String) in.readValue((String.class.getClassLoader())));
            this.loginType = ((String) in.readValue((String.class.getClassLoader())));
        }
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(userId);
            dest.writeValue(userName);
            dest.writeValue(userEmail);
            dest.writeValue(userMobile);
            dest.writeValue(userPass);
            dest.writeValue(userImg);
            dest.writeValue(userStatus);
            dest.writeValue(createdDate);
            dest.writeValue(modified);
            dest.writeValue(gender);
            dest.writeValue(dob);
            dest.writeValue(score);
            dest.writeValue(loginType);
        }

        public int describeContents() {
            return 0;
        }*/

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.userId);
            dest.writeString(this.userName);
            dest.writeString(this.userEmail);
            dest.writeString(this.userMobile);
            dest.writeString(this.userPass);
            dest.writeString(this.userImg);
            dest.writeString(this.userStatus);
            dest.writeString(this.createdDate);
            dest.writeString(this.modified);
            dest.writeString(this.gender);
            dest.writeString(this.dob);
            dest.writeString(this.score);
            dest.writeString(this.loginType);
            dest.writeString(this.socialId);
            dest.writeString(this.userCompany);
            dest.writeString(this.userDepartment);
            dest.writeString(this.userRole);
            dest.writeString(this.userBranch);
            dest.writeString(this.userLocation);
            dest.writeString(this.userRegion);
            dest.writeString(this.empNameForHome);
            dest.writeString(this.fullName);
            dest.writeString(this.userCountry);
        }

        public void readFromParcel(Parcel source) {
            this.userId = source.readString();
            this.userName = source.readString();
            this.userEmail = source.readString();
            this.userMobile = source.readString();
            this.userPass = source.readString();
            this.userImg = source.readString();
            this.userStatus = source.readString();
            this.createdDate = source.readString();
            this.modified = source.readString();
            this.gender = source.readString();
            this.dob = source.readString();
            this.score = source.readString();
            this.loginType = source.readString();
            this.socialId = source.readString();
            this.userCompany = source.readString();
            this.userDepartment = source.readString();
            this.userRole = source.readString();
            this.userBranch = source.readString();
            this.userLocation = source.readString();
            this.userRegion = source.readString();
            this.empNameForHome = source.readString();
            this.fullName = source.readString();
            this.userCountry = source.readString();
        }

        protected MyProfileListModel(Parcel in) {
            this.userId = in.readString();
            this.userName = in.readString();
            this.userEmail = in.readString();
            this.userMobile = in.readString();
            this.userPass = in.readString();
            this.userImg = in.readString();
            this.userStatus = in.readString();
            this.createdDate = in.readString();
            this.modified = in.readString();
            this.gender = in.readString();
            this.dob = in.readString();
            this.score = in.readString();
            this.loginType = in.readString();
            this.socialId = in.readString();
            this.userCompany = in.readString();
            this.userDepartment = in.readString();
            this.userRole = in.readString();
            this.userBranch = in.readString();
            this.userLocation = in.readString();
            this.userRegion = in.readString();
            this.empNameForHome = in.readString();
            this.fullName = in.readString();
            this.userCountry = in.readString();
        }

        public  final Parcelable.Creator<MyProfileListModel> CREATOR = new Parcelable.Creator<MyProfileListModel>() {
            @Override
            public MyProfileListModel createFromParcel(Parcel source) {
                return new MyProfileListModel(source);
            }

            @Override
            public MyProfileListModel[] newArray(int size) {
                return new MyProfileListModel[size];
            }
        };
    }
}
