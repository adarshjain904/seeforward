package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WelcomeMsg {
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    /*@Expose
    @SerializedName("data")
    private Data data;*/
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("response")
    private boolean response;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public static class Data {
      /*  @Expose
        @SerializedName("created_date")
        private String created_date;*/
        @Expose
        @SerializedName("page_content")
        private String page_content;
       /* @Expose
        @SerializedName("page_name")
        private String page_name;
        @Expose
        @SerializedName("page_id")
        private String page_id;*/
/*
        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }*/

        public String getPage_content() {
            return page_content;
        }

        public void setPage_content(String page_content) {
            this.page_content = page_content;
        }
/*
        public String getPage_name() {
            return page_name;
        }

        public void setPage_name(String page_name) {
            this.page_name = page_name;
        }*/
/*
        public String getPage_id() {
            return page_id;
        }

        public void setPage_id(String page_id) {
            this.page_id = page_id;
        }*/
    }
}
