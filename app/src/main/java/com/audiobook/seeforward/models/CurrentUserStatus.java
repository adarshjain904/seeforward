package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentUserStatus {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("response")
    private String response;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public static class Data {
        @Expose
        @SerializedName("audio")
        private String audio;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("listen_count")
        private String listen_count;
        @Expose
        @SerializedName("audio_id")
        private String audio_id;
        @Expose
        @SerializedName("exp_date")
        private String exp_date;
        @Expose
        @SerializedName("audio_history_id")
        private String audio_history_id;

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getListen_count() {
            return listen_count;
        }

        public void setListen_count(String listen_count) {
            this.listen_count = listen_count;
        }

        public String getAudio_id() {
            return audio_id;
        }

        public void setAudio_id(String audio_id) {
            this.audio_id = audio_id;
        }

        public String getExp_date() {
            return exp_date;
        }

        public void setExp_date(String exp_date) {
            this.exp_date = exp_date;
        }

        public String getAudio_history_id() {
            return audio_history_id;
        }

        public void setAudio_history_id(String audio_history_id) {
            this.audio_history_id = audio_history_id;
        }
    }
}
