package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpModel {
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private HelpListModel data;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HelpListModel getData() {
        return data;
    }

    public void setData(HelpListModel data) {
        this.data = data;
    }

    public class HelpListModel {
        @SerializedName("page_id")
        @Expose
        private String pageId;
        @SerializedName("page_name")
        @Expose
        private String pageName;
        @SerializedName("page_content")
        @Expose
        private String pageContent;
        @SerializedName("created_date")
        @Expose
        private String createdDate;

        public String getPageId() {
            return pageId;
        }

        public void setPageId(String pageId) {
            this.pageId = pageId;
        }

        public String getPageName() {
            return pageName;
        }

        public void setPageName(String pageName) {
            this.pageName = pageName;
        }

        public String getPageContent() {
            return pageContent;
        }

        public void setPageContent(String pageContent) {
            this.pageContent = pageContent;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

    }
}
