package com.audiobook.seeforward.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Audio implements Parcelable {
    @SerializedName("audio_history_id")
    private String audioHistoryId;
    @SerializedName("user_plan_id")
    private String userPlanId;
    @SerializedName("audio_id")
    private String audioId;
    @SerializedName("auto_active")
    private String autoActive;
    @SerializedName("listen_count")
    private String listenCount;
    @SerializedName("audio_enabled")
    private Boolean audioEnabled;
    @SerializedName("plan_expiry")
    private String planExpiry;
    @SerializedName("is_active")
    private String isActive;
    @SerializedName("audiolink")
    private String audiolink;
    @SerializedName("audio_title")
    private String audioTitle;
    public final static Parcelable.Creator<Audio> CREATOR = new Parcelable.Creator<Audio>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Audio createFromParcel(Parcel in) {
            return new Audio(in);
        }

        public Audio[] newArray(int size) {
            return (new Audio[size]);
        }

    };

    protected Audio(Parcel in) {
        this.audioHistoryId = ((String) in.readValue((String.class.getClassLoader())));
        this.userPlanId = ((String) in.readValue((String.class.getClassLoader())));
        this.audioId = ((String) in.readValue((String.class.getClassLoader())));
        this.autoActive = ((String) in.readValue((String.class.getClassLoader())));
        this.listenCount = ((String) in.readValue((String.class.getClassLoader())));
        this.audioEnabled = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.planExpiry = ((String) in.readValue((String.class.getClassLoader())));
        this.isActive = ((String) in.readValue((String.class.getClassLoader())));
        this.audiolink = ((String) in.readValue((String.class.getClassLoader())));
        this.audioTitle = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Audio() {
    }

    public String getAudioHistoryId() {
        return audioHistoryId;
    }

    public void setAudioHistoryId(String audioHistoryId) {
        this.audioHistoryId = audioHistoryId;
    }

    public Audio withAudioHistoryId(String audioHistoryId) {
        this.audioHistoryId = audioHistoryId;
        return this;
    }

    public String getUserPlanId() {
        return userPlanId;
    }

    public void setUserPlanId(String userPlanId) {
        this.userPlanId = userPlanId;
    }

    public Audio withUserPlanId(String userPlanId) {
        this.userPlanId = userPlanId;
        return this;
    }

    public String getAudioId() {
        return audioId;
    }

    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    public Audio withAudioId(String audioId) {
        this.audioId = audioId;
        return this;
    }

    public String getAutoActive() {
        return autoActive;
    }

    public void setAutoActive(String autoActive) {
        this.autoActive = autoActive;
    }

    public Audio withAutoActive(String autoActive) {
        this.autoActive = autoActive;
        return this;
    }

    public String getListenCount() {
        return listenCount;
    }

    public void setListenCount(String listenCount) {
        this.listenCount = listenCount;
    }

    public Audio withListenCount(String listenCount) {
        this.listenCount = listenCount;
        return this;
    }

    public Boolean getAudioEnabled() {
        return audioEnabled;
    }

    public void setAudioEnabled(Boolean audioEnabled) {
        this.audioEnabled = audioEnabled;
    }

    public Audio withAudioEnabled(Boolean audioEnabled) {
        this.audioEnabled = audioEnabled;
        return this;
    }

    public String getPlanExpiry() {
        return planExpiry;
    }

    public void setPlanExpiry(String planExpiry) {
        this.planExpiry = planExpiry;
    }

    public Audio withPlanExpiry(String planExpiry) {
        this.planExpiry = planExpiry;
        return this;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Audio withIsActive(String isActive) {
        this.isActive = isActive;
        return this;
    }

    public String getAudiolink() {
        return audiolink;
    }

    public void setAudiolink(String audiolink) {
        this.audiolink = audiolink;
    }

    public Audio withAudiolink(String audiolink) {
        this.audiolink = audiolink;
        return this;
    }

    public String getAudioTitle() {
        return audioTitle;
    }

    public void setAudioTitle(String audioTitle) {
        this.audioTitle = audioTitle;
    }

    public Audio withAudioTitle(String audioTitle) {
        this.audioTitle = audioTitle;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(audioHistoryId);
        dest.writeValue(userPlanId);
        dest.writeValue(audioId);
        dest.writeValue(autoActive);
        dest.writeValue(listenCount);
        dest.writeValue(audioEnabled);
        dest.writeValue(planExpiry);
        dest.writeValue(isActive);
        dest.writeValue(audiolink);
        dest.writeValue(audioTitle);
    }

    public int describeContents() {
        return 0;
    }
}
