package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class AllQuestion {

    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("response")
    private boolean response;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public static class Data {
        @Expose
        @SerializedName("marks5")
        private String marks5;

        public String getMarks5() {
            return marks5;
        }

        public void setMarks5(String marks5) {
            this.marks5 = marks5;
        }

        @Expose
        @SerializedName("marks4")
        private String marks4;
        @Expose
        @SerializedName("marks3")
        private String marks3;
        @Expose
        @SerializedName("marks2")
        private String marks2;
        @Expose
        @SerializedName("answer5")
        private String answer5;

        public String getAnswer5() {
            return answer5;
        }

        public void setAnswer5(String answer5) {
            this.answer5 = answer5;
        }

        @Expose
        @SerializedName("answer4")
        private String answer4;
        @Expose
        @SerializedName("answer3")
        private String answer3;
        @Expose
        @SerializedName("answer2")
        private String answer2;
        @Expose
        @SerializedName("marks1")
        private String marks1;
        @Expose
        @SerializedName("answer1")
        private String answer1;
        @Expose
        @SerializedName("choose_option")
        private String choose_option;
        @Expose
        @SerializedName("question")
        private String question;
        @Expose
        @SerializedName("q_id")
        private String q_id;

        public String getMarks4() {
            return marks4;
        }

        public void setMarks4(String marks4) {
            this.marks4 = marks4;
        }

        public String getMarks3() {
            return marks3;
        }

        public void setMarks3(String marks3) {
            this.marks3 = marks3;
        }

        public String getMarks2() {
            return marks2;
        }

        public void setMarks2(String marks2) {
            this.marks2 = marks2;
        }

        public String getAnswer4() {
            return answer4;
        }

        public void setAnswer4(String answer4) {
            this.answer4 = answer4;
        }

        public String getAnswer3() {
            return answer3;
        }

        public void setAnswer3(String answer3) {
            this.answer3 = answer3;
        }

        public String getAnswer2() {
            return answer2;
        }

        public void setAnswer2(String answer2) {
            this.answer2 = answer2;
        }

        public String getMarks1() {
            return marks1;
        }

        public void setMarks1(String marks1) {
            this.marks1 = marks1;
        }

        public String getAnswer1() {
            return answer1;
        }

        public void setAnswer1(String answer1) {
            this.answer1 = answer1;
        }

        public String getChoose_option() {
            return choose_option;
        }

        public void setChoose_option(String choose_option) {
            this.choose_option = choose_option;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getQ_id() {
            return q_id;
        }

        public void setQ_id(String q_id) {
            this.q_id = q_id;
        }
    }
}
