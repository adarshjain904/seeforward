package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentSuccess {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("response")
    private String response;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public static class Data {
        @Expose
        @SerializedName("post_date")
        private String post_date;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("days")
        private String days;
        @Expose
        @SerializedName("start_date")
        private String start_date;
        @Expose
        @SerializedName("exp_date")
        private String exp_date;
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("plan_name")
        private String plan_name;
        @Expose
        @SerializedName("order_id")
        private String order_id;
        @Expose
        @SerializedName("payment_detail_id")
        private String payment_detail_id;

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getExp_date() {
            return exp_date;
        }

        public void setExp_date(String exp_date) {
            this.exp_date = exp_date;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getPayment_detail_id() {
            return payment_detail_id;
        }

        public void setPayment_detail_id(String payment_detail_id) {
            this.payment_detail_id = payment_detail_id;
        }
    }
}
