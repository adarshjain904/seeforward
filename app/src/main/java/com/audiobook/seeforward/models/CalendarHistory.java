package com.audiobook.seeforward.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CalendarHistory {
    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("data")
    @Expose
    private List<CalendarData> calendarDataList;

    @SerializedName("today")
    @Expose
    private List<TodayData> todayDataList;

    @SerializedName("total")
    @Expose
    private String total;


    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setCalendarDataList(List<CalendarData> calendarDataList) {
        this.calendarDataList = calendarDataList;
    }

    public void setTodayDataList(List<TodayData> todayDataList) {
        this.todayDataList = todayDataList;
    }

    public String getResponse() {
        return response;
    }

    public List<CalendarData> getCalendarDataList() {
        return calendarDataList;
    }

    public List<TodayData> getTodayDataList() {
        return todayDataList;
    }

    public static class CalendarData {
        @SerializedName("audio_listen_history_id")
        @Expose
        private String audioListenHistoryId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("audio_history_id")
        @Expose
        private String audioHistoryId;
        @SerializedName("audio_id")
        @Expose
        private String audioId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getAudioListenHistoryId() {
            return audioListenHistoryId;
        }

        public void setAudioListenHistoryId(String audioListenHistoryId) {
            this.audioListenHistoryId = audioListenHistoryId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getAudioHistoryId() {
            return audioHistoryId;
        }

        public void setAudioHistoryId(String audioHistoryId) {
            this.audioHistoryId = audioHistoryId;
        }

        public String getAudioId() {
            return audioId;
        }

        public void setAudioId(String audioId) {
            this.audioId = audioId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }

    public static class TodayData {
        @SerializedName("cid")
        @Expose
        private String cId;

        @SerializedName("user_id")
        @Expose
        private String userId;

        @SerializedName("audio_id")
        @Expose
        private String audioId;

        @SerializedName("audio_history_id")
        @Expose
        private String audioHistoryId;

        @SerializedName("cdate")
        @Expose
        private String cDate;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("level")
        @Expose
        private String level;

        @SerializedName("audio")
        @Expose
        private String audio;

        @SerializedName("points")
        @Expose
        private String points;

        @SerializedName("title")
        @Expose
        private String title;

        public void setcId(String cId) {
            this.cId = cId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setAudioId(String audioId) {
            this.audioId = audioId;
        }

        public void setAudioHistoryId(String audioHistoryId) {
            this.audioHistoryId = audioHistoryId;
        }

        public void setcDate(String cDate) {
            this.cDate = cDate;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getcId() {
            return cId;
        }

        public String getUserId() {
            return userId;
        }

        public String getAudioId() {
            return audioId;
        }

        public String getAudioHistoryId() {
            return audioHistoryId;
        }

        public String getcDate() {
            return cDate;
        }

        public String getId() {
            return id;
        }

        public String getLevel() {
            return level;
        }

        public String getAudio() {
            return audio;
        }

        public String getPoints() {
            return points;
        }

        public String getTitle() {
            return title;
        }
    }
}