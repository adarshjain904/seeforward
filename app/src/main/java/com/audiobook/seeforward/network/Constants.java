package com.audiobook.seeforward.network;

public class Constants {

    public static final String url = "https://www.seeforward.co.uk/AudioBook/api/";
    public static final String ImageBaseurl = "https://www.seeforward.co.uk/AudioBook/";

    public static final String KEY_LOGIN_STATUS = "loginstatus";
    public static final String KEY_USER_ID = "userid";
    public static final String KEY_USER_IMAGE = "userimage";
    public static final String KEY_USER_NAME = "username";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_LOGIN_TYPE = "login_type";

}