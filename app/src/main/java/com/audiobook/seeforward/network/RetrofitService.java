package com.audiobook.seeforward.network;

import com.audiobook.seeforward.models.AllQuestion;
import com.audiobook.seeforward.models.AudioData;
import com.audiobook.seeforward.models.CalendarHistory;
import com.audiobook.seeforward.models.ChangePassword;
import com.audiobook.seeforward.models.CheckAudioStatus;
import com.audiobook.seeforward.models.CurrentUserStatus;
import com.audiobook.seeforward.models.FeedbackModel;
import com.audiobook.seeforward.models.ForgetPassword;
import com.audiobook.seeforward.models.HelpModel;
import com.audiobook.seeforward.models.LoginModel;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.models.PaymentSuccess;
import com.audiobook.seeforward.models.PreviousScore;
import com.audiobook.seeforward.models.WelcomeMsg;
import com.google.gson.JsonElement;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitService {

    @Multipart
    @POST("signup")
    Call<JsonElement> submitRegister(@Part MultipartBody.Part profile_pic,
                                     @Part("user_name") RequestBody user_name,
                                     @Part("user_email") RequestBody user_email,
                                     @Part("user_pass") RequestBody user_pass,
                                     @Part("user_mobile") RequestBody user_mobile);//correct

    @Multipart
    @POST("comapnysignup")
    Call<JsonElement> comapnysignupRegister(@Part MultipartBody.Part profile_pic,
                                            @Part("user_name") RequestBody user_name,
                                            @Part("user_email") RequestBody user_email,
                                            @Part("user_pass") RequestBody user_pass,
                                            @Part("user_mobile") RequestBody user_mobile,
                                            @Part("emp_name_for_home") RequestBody emp_name_for_home,
                                            @Part("unique_number") RequestBody unique_number,
                                            @Part("user_role") RequestBody user_role);//correct

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("user_email") String username, @Field("user_pass") String password, @Field("user_type") String userType);//correct

    @FormUrlEncoded
    @POST("facebook_login")
    Call<LoginModel> loginWithFacebook(@Field("user_name") String username, @Field("user_email") String email, @Field("social_id") String social_id,
                                       @Field("user_mobile") String mobile, @Field("image") String image); //correct


    @GET("WelcomePage")
    Call<WelcomeMsg> getWelcomeMsg();//correct


    @FormUrlEncoded
    @POST("myprofile")
    Call<MyProfileModel> MyProfileApi(@Field("user_id") String user_id);//correct

    @FormUrlEncoded
    @POST("check_show_question")
    Call<JsonElement> checkShowQuestion(@Field("user_id") String user_id);//correct


    @Multipart
    @POST("UserEdit")
    Call<JsonElement> updateProfileApi(@Part MultipartBody.Part profile_pic,
                                       @Part("user_id") RequestBody user_id,
                                       @Part("user_name") RequestBody user_name,
                                       @Part("user_email") RequestBody user_email,
                                       @Part("user_pass") RequestBody user_pass,
                                       @Part("gender") RequestBody gender,
                                       @Part("user_mobile") RequestBody user_mobile,
                                       @Part("dob") RequestBody dob,
                                       @Part("user_company") RequestBody user_company,
                                       @Part("user_department") RequestBody user_department,
                                       @Part("user_role") RequestBody user_role,
                                       @Part("user_branch") RequestBody user_branch,
                                       @Part("user_region") RequestBody user_region,
                                       @Part("user_location") RequestBody user_location,
                                       @Part("user_country") RequestBody user_country);//correct

    @GET("help")
    Call<HelpModel> getHelpApi();//correct

    @FormUrlEncoded
    @POST("feedback")
    Call<FeedbackModel> FeedbackApi(@Field("name") String name, @Field("email") String email, @Field("message") String message, @Field("user_id") String user_id);//correct

    @FormUrlEncoded
    @POST("post_testscore")
    Call<AudioData> postTestScore(@Field("user_id") String user_id, @Field("total") String total);//correct

    @FormUrlEncoded
    @POST("forget_password")
    Call<ForgetPassword> forgetPasswordEmail(@Field("emailid") String email);//correct

    @FormUrlEncoded
    @POST("change_password")
    Call<ChangePassword> changePassword(@Field("oldpass") String oldPassword, @Field("new_pwd") String newPassword,
                                        @Field("con_pwd") String confirmPassword, @Field("userid") String userId);//correct

    @FormUrlEncoded
    @POST("a_audio_listen_count")
    Call<ResponseBody> postAudioCount(@Field("user_id") String userId,
                                      @Field("audio_history_id") String audioHistoryId,
                                      @Field("audio_id") String nextAudioHistoryId);//correct

    @FormUrlEncoded
    @POST("subscribe_free_packages")
    Call<ResponseBody> subscribeFreePackagesApi(@Field("user_id") String user_id, @Field("plan_id") String planid);//correct

    @FormUrlEncoded
    @POST("calender_histroy")
    Call<CalendarHistory> getCalendarHistory(@Field("user_id") String userId, @Field("sdate") String selectedDate);//correct

    @FormUrlEncoded
    @POST("a_getplanlist")
    Call<ResponseBody> getAudioList(@Field("user_id") String userId);//correct

    @POST("all_Que_ans")
    Call<AllQuestion> getQuestions();//correct

    @FormUrlEncoded
    @POST("get_last_score")
    Call<PreviousScore> getLastScore(@Field("user_id") String userId);//correct

    @FormUrlEncoded
    @POST("a_post_payment")
    Call<ResponseBody> postPaymentApi(@Field("user_id") String user_id,
                                      @Field("plan_id") String planid,
                                      @Field("amount") String amount,
                                      @Field("total_score") String total_score);//correct

    @FormUrlEncoded
    @POST("check_show_question_update")
    Call<JsonElement> checkShowQuestionUpdate(@Field("manage_question_id") String manage_question_id); //correct

    @FormUrlEncoded
    @POST("Post_payment")
    Call<PaymentSuccess> paymentApi(@Field("user_id") String user_id, @Field("plan_name") String plan_name,
                                    @Field("days") String days, @Field("amount") String amount,
                                    @Field("order_id") String order_id, @Field("audio_id") String audio_id,
                                    @Field("total_score") String total_score, @Field("all_audioid") String allAudioId);


    @FormUrlEncoded
    @POST("change_expiry_date")
    Call<CheckAudioStatus> unlockAnotherProgram(@Field("preexpirydate") String previousExpiryDate,
                                                @Field("next_audio_history_id") String nextAudioHistoryId);

    @FormUrlEncoded
    @POST("a_audio_listen_histroy")
    Call<ResponseBody> getListenCountByDate(@Field("user_id") String userId, @Field("date") String selectedDate);


    @FormUrlEncoded
    @POST("check_user_audio_status")
    Call<CheckAudioStatus> checkAudioStatus(@Field("user_id") String userId,
                                            @Field("audio_history_id") String audioHistoryId,
                                            @Field("nextaudio_history_id") String nextAudioHistoryId);

    @FormUrlEncoded
    @POST("a_audio_listen_histroy")
    Call<ResponseBody> getCalendarHistoryUpdated(@Field("user_id") String userId,
                                                 @Field("date") String audioHistoryId);

    @FormUrlEncoded
    @POST("Get_dashboard_status")
    Call<CurrentUserStatus> checkStatus(@Field("user_id") String user_id);
}