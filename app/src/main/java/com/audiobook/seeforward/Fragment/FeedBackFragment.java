package com.audiobook.seeforward.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.models.FeedbackModel;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class FeedBackFragment extends Fragment implements View.OnClickListener {
    EditText nameFeedback, emailFeedback, messageFeedback;
    TextView sendBtn;
    RetrofitService apiInterface;
    String user_id;
    String mNameFeedback, mEmailFeedback, mMessageFeedback;

    public FeedBackFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_back, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        nameFeedback = view.findViewById(R.id.namefeedback);
        emailFeedback = view.findViewById(R.id.emailfeedback);
        messageFeedback = view.findViewById(R.id.messagefeedback);
        sendBtn = view.findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendBtn:
                mNameFeedback = nameFeedback.getText().toString();
                mEmailFeedback = emailFeedback.getText().toString();
                mMessageFeedback = messageFeedback.getText().toString();
                if (Utility.isNetAvailable(getActivity())){
                    FeedbackApi(mNameFeedback, mEmailFeedback, mMessageFeedback, user_id);
                }else {
                    Toast.makeText(getActivity(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void FeedbackApi(String name, String email, String message, String user_id) {
        apiInterface.FeedbackApi(name, email, message, user_id).enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
                if (response.isSuccessful()) {
                    FeedbackModel feedbackModel = response.body();
                    String responseapi = feedbackModel.getResponse();
                    String message = feedbackModel.getMessage();
                    if (responseapi.equals("true")) {
                        Toast.makeText(getActivity(), "Feedback sent", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), LandingScreenActivity.class));
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }
}