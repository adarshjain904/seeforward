package com.audiobook.seeforward.Fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.audiobook.seeforward.Adapter.HistoryListAdapter;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.AudioData;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    ProgressBar progress_bar;
    RecyclerView rc_history;

    RetrofitService apiInterface;
    String user_id;
    HistoryListAdapter historyListAdapter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        rc_history = view.findViewById(R.id.rc_history);
        progress_bar = view.findViewById(R.id.progress_bar);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rc_history.setLayoutManager(layoutManager);

        historyListAdapter = new HistoryListAdapter(getActivity());
        rc_history.setAdapter(historyListAdapter);

        if (Utility.isNetAvailable(getActivity())) {
            sendScoreToserver("25");
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_network), Toast.LENGTH_SHORT).show();
        }


        return view;
    }

    private void sendScoreToserver(String score) {
        progress_bar.setVisibility(View.VISIBLE);


        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<AudioData> call = api.postTestScore(SharedPrefsManager.getInstance().getString(KEY_USER_ID), score);
        call.enqueue(new Callback<AudioData>() {
            @Override
            public void onResponse(Call<AudioData> call, Response<AudioData> response) {
                Log.e("response", response.body().toString());
                progress_bar.setVisibility(View.GONE);
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AudioData audioData = response.body();
                    List<AudioData.AudioDataList> audioDataLists = audioData.getAudioDataLists();
                    Log.i("rhl...", String.valueOf(audioDataLists.size()));
                    if (audioDataLists != null && audioDataLists.size() > 0) {
                        historyListAdapter.addCustomerList(audioDataLists);
                    }

                } else {
                    Toast.makeText(getContext(), "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AudioData> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);

            }
        });
    }


}
