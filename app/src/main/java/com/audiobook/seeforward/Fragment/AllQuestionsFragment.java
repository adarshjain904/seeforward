package com.audiobook.seeforward.Fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.MyProgrammesActivity;
import com.audiobook.seeforward.models.AllQuestion;
import com.audiobook.seeforward.models.AudioData;
import com.audiobook.seeforward.models.PreviousScore;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllQuestionsFragment extends Fragment {
    /*@BindView(R.id.progress_bar)
    ProgressBar progressBar;*/
    @BindView(R.id.ans1)
    TextView ans1;
    @BindView(R.id.ans2)
    TextView ans2;
    @BindView(R.id.ans3)
    TextView ans3;
    @BindView(R.id.ans4)
    TextView ans4;
    @BindView(R.id.ans5)
    TextView ans5;
    @BindView(R.id.question)
    TextView question;
    @BindView(R.id.next)
    TextView next;

    @BindView(R.id.rl_button)
    RelativeLayout rl_button;
    int index = 1;
    public List<AllQuestion.Data> dataList;
    @BindView(R.id.back3)
    RelativeLayout back3;
    @BindView(R.id.back5)
    RelativeLayout back5;
    @BindView(R.id.back4)
    RelativeLayout back4;
    @BindView(R.id.back2)
    RelativeLayout back2;
    @BindView(R.id.back1)
    RelativeLayout back1;
    @BindView(R.id.count1)
    TextView count1;
    @BindView(R.id.count2)
    TextView count2;
    @BindView(R.id.count3)
    TextView count3;
    @BindView(R.id.count4)
    TextView count4;
    @BindView(R.id.count5)
    TextView count5;
    private int allScore = 0;

    private String mPreviousScore = "";
    private int mSelectedAnswer = 0;

    @BindView(R.id.quesLayout)
    LinearLayout quesLayout;
    private boolean isSelected = false;
    private Dialog dialog, mDialog;

    private Context mContext;

    public AllQuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_questions, container, false);
        mContext = getContext();
        ButterKnife.bind(this, view);

        ProgressDailog();
        if (Utility.isNetAvailable(mContext)) {
            getAllQueations();
            getPreviousScore(SharedPrefsManager.getInstance().getString(KEY_USER_ID));
        } else {
            Toast.makeText(mContext, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }

        setHasOptionsMenu(true);
        return view;
    }

    private void getAllQueations() {
        dialog.show();
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<AllQuestion> call = api.getQuestions();
        call.enqueue(new Callback<AllQuestion>() {
            @Override
            public void onResponse(Call<AllQuestion> call, Response<AllQuestion> response) {
                //  Log.e("response", response.body().toString());
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AllQuestion allQuestion = response.body();
                    dataList = allQuestion.getData();
                    question.setText(allQuestion.getData().get(0).getQuestion());
                    if (allQuestion.getData().get(0).getChoose_option().equalsIgnoreCase("2")) {
                        ans1.setText(allQuestion.getData().get(0).getAnswer1());
                        ans2.setText(allQuestion.getData().get(0).getAnswer2());
                    } else {
                        ans1.setText(allQuestion.getData().get(0).getAnswer1());
                        ans2.setText(allQuestion.getData().get(0).getAnswer2());
                        ans3.setText(allQuestion.getData().get(0).getAnswer3());
                        ans4.setText(allQuestion.getData().get(0).getAnswer4());
                        ans5.setText(allQuestion.getData().get(0).getAnswer5());
                    }
                    dialog.dismiss();
                    //progressBar.setVisibility(View.GONE);
                    quesLayout.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getActivity(), "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllQuestion> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                // progressBar.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.next)
    public void onViewClicked() {
        countScore(index);
        showScoreDialog();
    }

    private void setBackgroundAll() {
        back1.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
        back2.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
        back3.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
        back4.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
        back5.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));

        count1.setTextColor(getResources().getColor(R.color.white));
        count2.setTextColor(getResources().getColor(R.color.white));
        count3.setTextColor(getResources().getColor(R.color.white));
        count4.setTextColor(getResources().getColor(R.color.white));
        count5.setTextColor(getResources().getColor(R.color.white));
    }

    private void setQuestions(int index) {
        countScore(index);
        question.setText(dataList.get(index).getQuestion());
        if (dataList.get(index).getChoose_option().equalsIgnoreCase("2")) {
            ans1.setText(dataList.get(index).getAnswer1());
            ans2.setText(dataList.get(index).getAnswer2());
            back3.setVisibility(View.GONE);
            back4.setVisibility(View.GONE);
        } else {
            back3.setVisibility(View.VISIBLE);
            back4.setVisibility(View.VISIBLE);
            ans1.setText(dataList.get(index).getAnswer1());
            ans2.setText(dataList.get(index).getAnswer2());
            ans3.setText(dataList.get(index).getAnswer3());
            ans4.setText(dataList.get(index).getAnswer4());
            ans5.setText(dataList.get(index).getAnswer5());
        }
        this.index++;
        isSelected = false;
    }

    private void countScore(int index) {
        switch (mSelectedAnswer) {
            case 1:
                allScore = allScore + Integer.parseInt(dataList.get(index - 1).getMarks1());
                Log.e("marks 1", dataList.get(index - 1).getMarks1());
                break;

            case 2:
                allScore = allScore + Integer.parseInt(dataList.get(index - 1).getMarks2());
                Log.e("marks 2", dataList.get(index - 1).getMarks2());
                break;

            case 3:
                allScore = allScore + Integer.parseInt(dataList.get(index - 1).getMarks3());
                Log.e("marks 3", dataList.get(index - 1).getMarks3());
                break;

            case 4:
                allScore = allScore + Integer.parseInt(dataList.get(index - 1).getMarks4());
                Log.e("marks 4", dataList.get(index - 1).getMarks4());
                break;

            case 5:
                allScore = allScore + Integer.parseInt(dataList.get(index - 1).getMarks5());
                Log.e("marks 5", dataList.get(index - 1).getMarks5());
                break;
        }

        Log.e(index + "Total score=" + dataList.get(index - 1).getQ_id(), String.valueOf(allScore));
    }

    @OnClick({R.id.back1, R.id.back2, R.id.back3, R.id.back4, R.id.back5})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back1:
                setBackground(back1, count1);
                break;
            case R.id.back2:
                setBackground(back2, count2);
                break;
            case R.id.back3:
                setBackground(back3, count3);
                break;
            case R.id.back4:
                setBackground(back4, count4);
                break;
            case R.id.back5:
                setBackground(back5, count5);
                break;
        }
    }

    private void setBackground(RelativeLayout ans, TextView count) {
        isSelected = true;
        count.setTextColor(getResources().getColor(R.color.black));
        ans.setBackground(getActivity().getDrawable(R.drawable.greenquestionboder));
        if (ans.getId() == R.id.back1) {
            back2.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back5.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));

            count2.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
            count5.setTextColor(getResources().getColor(R.color.white));

            mSelectedAnswer = 1;

        } else if (ans.getId() == R.id.back2) {
            back1.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back5.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));

            count1.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
            count5.setTextColor(getResources().getColor(R.color.white));

            mSelectedAnswer = 2;

        } else if (ans.getId() == R.id.back3) {
            back1.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back2.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back5.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            count2.setTextColor(getResources().getColor(R.color.white));
            count1.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
            count5.setTextColor(getResources().getColor(R.color.white));

            mSelectedAnswer = 3;
        } else if (ans.getId() == R.id.back4) {
            back1.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back2.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back5.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));

            count2.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count1.setTextColor(getResources().getColor(R.color.white));
            count5.setTextColor(getResources().getColor(R.color.white));

            mSelectedAnswer = 4;
        } else if (ans.getId() == R.id.back5) {
            back1.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back2.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back3.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));
            back4.setBackground(getActivity().getDrawable(R.drawable.whitequestionboder));

            count2.setTextColor(getResources().getColor(R.color.white));
            count3.setTextColor(getResources().getColor(R.color.white));
            count4.setTextColor(getResources().getColor(R.color.white));
            count1.setTextColor(getResources().getColor(R.color.white));

            mSelectedAnswer = 5;
        }

        //adarsh 31/12/2020
        if (dataList.size() == index) {
            dialog.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("submit", "SUBMIT   " + allScore);
                    dialog.dismiss();
                    rl_button.setVisibility(View.VISIBLE);
                }
            }, 700);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    count1.setTextColor(getResources().getColor(R.color.white));
                    setBackgroundAll();
                    setQuestions(index);
                }
            }, 100);
        }
    }

    private void ProgressDailog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(mContext).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onDestroy() {
        dialog.dismiss();
        super.onDestroy();
    }


    private void showScoreDialog() {
        mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.dialog_score);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button scoreNextBtn = mDialog.findViewById(R.id.score_next_btn);

        TextView scoreTxtView = mDialog.findViewById(R.id.score_txt_view);
        scoreTxtView.setText(String.valueOf(allScore / 2) + " out of 100");

        TextView previousScoreTxtView = mDialog.findViewById(R.id.previous_score_txt_view);
        TextView scoreTitleTxtView = mDialog.findViewById(R.id.score_title_txt_view);


        if (mPreviousScore != null && !TextUtils.isEmpty(mPreviousScore)) {
            scoreTitleTxtView.setVisibility(View.GONE);
            scoreTxtView.setVisibility(View.GONE);
            previousScoreTxtView.setVisibility(View.VISIBLE);
            previousScoreTxtView.setText("Congratulations on completing this programme. Your initial score was " + mPreviousScore
                    + " and your score now is " + (allScore / 2) + ".");
        } else {
            scoreTitleTxtView.setVisibility(View.VISIBLE);
            scoreTxtView.setVisibility(View.VISIBLE);
            previousScoreTxtView.setVisibility(View.GONE);
        }

        scoreNextBtn.setOnClickListener(v -> {
                    if (Utility.isNetAvailable(getActivity())) {
                        postScoreOnServer(allScore / 2, mDialog);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }


                }
        );
        mDialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDialog != null)
            if (mDialog.isShowing()) {
                postScoreOnServer(allScore / 2, mDialog);
                mDialog.dismiss();
            }
    }

    private void postScoreOnServer(int i, Dialog dialog) {
        Call<AudioData> call = RetrofitClient.getInterface().postTestScore(SharedPrefsManager.getInstance().getString(KEY_USER_ID), String.valueOf(i));
        call.enqueue(new Callback<AudioData>() {
            @Override
            public void onResponse(Call<AudioData> call, Response<AudioData> response) {
                Log.e("response", response.body().toString());
                dialog.dismiss();
                dialog.dismiss();
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AudioData audioData = response.body();
                    AudioData.UserDeatailsData userDeatailsData = audioData.getUser_details();

                    if (SharedPrefsManager.getInstance().getString(Utility.PREF_DAY_DATA).equals("0")) {
                        SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, String.valueOf(i));
                        AllQuestionsFragment.this.dialog.dismiss();
                        Intent intent = new Intent(getContext(), MyProgrammesActivity.class);
                        intent.putExtra("allScore", String.valueOf(allScore / 2));
                        startActivity(intent);
                        // getActivity().finish();
                    } else {
                        checkdays(i);
                    }
                } else {
                    Toast.makeText(mContext, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AudioData> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }


    private void checkdays(int i) {
        RetrofitClient.getInterface().checkShowQuestionUpdate(SharedPrefsManager.getInstance().getString(Utility.PREF_DAY_DATA)).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.d("response", response.body().toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, String.valueOf(i));
                    AllQuestionsFragment.this.dialog.dismiss();
                    Intent intent = new Intent(getContext(), MyProgrammesActivity.class);
                    intent.putExtra("allScore", String.valueOf(allScore / 2));
                    startActivity(intent);
                    getActivity().finish();


                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
    }


    private void getPreviousScore(String userId) {
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        api.getLastScore(userId).enqueue(new Callback<PreviousScore>() {
            @Override
            public void onResponse(Call<PreviousScore> call, Response<PreviousScore> response) {
                if (response.isSuccessful()) {
                    PreviousScore previousScore = response.body();
                    if (previousScore != null) {
                        if (previousScore.getResponse().equals("true")) {
                            mPreviousScore = previousScore.getPreviousScoreData().getTotalScore();
                            Log.e("mPreviousScore=-=",mPreviousScore);
                        } else {
                            //Toast.makeText(AllQuestionActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PreviousScore> call, Throwable t) {

            }
        });
    }

}

    /*
    public void showInfoDialog() {
        Dialog mDialog2 = new Dialog(getContext());
        mDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog2.setContentView(R.layout.info_dailog);

        LinearLayout cancel = mDialog2.findViewById(R.id.okay);

        mDialog2.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(mContext, "ok done", Toast.LENGTH_SHORT).show();

                mDialog2.hide();

            }
        });
    }*/
/*

    private void sendScoreToserver(String score) {
        showDialog();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                //.addConverterFactory(GsonConverterFactory.create())
                .build();

        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = retrofit.create(RetrofitService.class);
        Call<AudioData> call = api.getAudio(SharedPrefsManager.getInstance().getString(KEY_USER_ID),score);
        call.enqueue(new Callback<AudioData>() {
            @Override
            public void onResponse(Call<AudioData> call, Response<AudioData> response) {
                Log.e("response", response.body().toString());

                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    AudioData audioData = response.body();
                    AudioData.Data data = audioData.getData();

                    Intent intent=new Intent(getActivity(), AudioActivity.class);
                    intent.putExtra("audioFilePath",data.getAudiofile());
                    intent.putExtra("audioFileName",data.getAudiotitle());
                    dialog.dismiss();
                    startActivity(intent);
                    //getActivity().finish();

                } else {
                    Toast.makeText(getActivity(), "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AudioData> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                // progressBar.setVisibility(View.GONE);
            }
        });
    }
*/