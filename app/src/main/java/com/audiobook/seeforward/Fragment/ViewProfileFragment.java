package com.audiobook.seeforward.Fragment;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.audiobook.seeforward.profile.EditProfileActivity;
import com.audiobook.seeforward.utiles.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.login.ChangePasswordActivity;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.network.Constants;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_TYPE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewProfileFragment extends Fragment {
    TextView editprofile;
    ImageView userImage;
    TextView userName, userEmail;
    RetrofitService apiInterface;
    String user_id;
    private Dialog dialog;

    private String mLoginType;

    public ViewProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);
        mLoginType = SharedPrefsManager.getInstance().getString(KEY_LOGIN_TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_profile, container, false);
        ProgressDailog();
        if (Utility.isNetAvailable(getActivity())){
            MyProfileApi(user_id);
        }else {
            Toast.makeText(getActivity(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }

        init(view);
        return view;
    }

    public void init(View view) {
        editprofile = view.findViewById(R.id.edit_profile);
        userName = view.findViewById(R.id.username);
        userEmail = view.findViewById(R.id.useremail);
        userImage = view.findViewById(R.id.userimage);

        TextView changePasswordBtn = view.findViewById(R.id.change_password_btn);

        if (mLoginType != null && mLoginType.equals("facebook")) {
            changePasswordBtn.setVisibility(View.GONE);
        } else {
            changePasswordBtn.setVisibility(View.VISIBLE);
        }

        changePasswordBtn.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
            startActivity(intent);
        });

        editprofile.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), EditProfileActivity.class);
            startActivity(intent);
        });
    }

    private void MyProfileApi(String user_id) {
        showDialog();
        apiInterface.MyProfileApi(user_id).enqueue(new Callback<MyProfileModel>() {
            @Override
            public void onResponse(Call<MyProfileModel> call, Response<MyProfileModel> response) {
                if (response.isSuccessful()) {
                    cancleDialog();
                    MyProfileModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        MyProfileModel.MyProfileListModel userdata1 = userdata.getData();
                        String username = userdata1.getUserName();
                        String useremail = userdata1.getUserEmail();
                        String userimage = userdata1.getUserImg();
                        String userId = userdata1.getUserId();

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.usereditimage)
                                .error(R.drawable.usereditimage)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH).dontAnimate()
                                .dontTransform();
                        SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, userimage);
                        Glide.with(getActivity()).load(Constants.ImageBaseurl + userimage).apply(options).into(userImage);
                        userName.setText(username);
                        userEmail.setText(useremail);
                    }
                } else {
                    cancleDialog();
                    Toast.makeText(getActivity(), getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyProfileModel> call, Throwable t) {
                cancleDialog();
                Toast.makeText(getActivity(), getString(R.string.error_network), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ProgressDailog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(getActivity()).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }
}
