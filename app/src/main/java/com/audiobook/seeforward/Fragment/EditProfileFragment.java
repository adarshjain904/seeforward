package com.audiobook.seeforward.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_EMAIL;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_NAME;

public class EditProfileFragment extends Fragment implements View.OnClickListener {
    EditText fullName, lastName, Dob, email, password, mobileNo;
    ImageView userImage;
    RadioGroup genderradioGroup;
    TextView saveBtn;
    Dialog settingsDialog;
    public static int FROM_GALLERY = 1;
    public static int FROM_CAMERA = 2;
    String pathProfile = null;
    Uri filePath;
    Bitmap bitmap;
    File file;
    final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    final int READ_PERMISSION_REQUEST_CODE = 4;
    final int WRITE_PERMISSION_REQUEST_CODE = 5;
    private String s_profilepic = "";
    ProgressDialog progressDialog;
    String mUserName, mUserlastName, mUserEmail, mUserPassword;
    TextView signUpContinueBtn;
    String user_id, mDob, s_radio2, mMobileNo;
    RadioButton reg_woman, reg_man;
    RadioGroup radioGroup;
    String dobDate, dobMonth, dobYear;
    RetrofitService apiInterface;
    private Dialog dialog;
    private RadioButton rb1;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);
        Log.e("userid", user_id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ProgressDailog();
        initView(view);
        return view;
    }

    public void initView(View view) {
        mobileNo = view.findViewById(R.id.mobileNo);
        fullName = view.findViewById(R.id.firstname);
        lastName = view.findViewById(R.id.lastname);
        Dob = view.findViewById(R.id.selectbirthday_etv);
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        saveBtn = view.findViewById(R.id.SaveBtn);
        userImage = view.findViewById(R.id.userimage);
        //    genderradioGroup = view.findViewById(R.id.radiogrp);
        radioGroup = (RadioGroup) view.findViewById(R.id.radiogrp);
        reg_woman = (RadioButton) view.findViewById(R.id.rb_woman);
        reg_man = (RadioButton) view.findViewById(R.id.rb_man);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                int selected = radioGroup.getCheckedRadioButtonId();
                rb1 = (RadioButton) group.findViewById(selected);
                s_radio2 = rb1.getText().toString();
                Log.i("rhl...!!", "come");
                Log.i("rhl...!!", String.valueOf(rb1).toString());
            }
        });


        if (rb1 == null) {
            reg_man.setChecked(true);
            Log.i("rhl...@", String.valueOf(rb1));
            s_radio2 = reg_man.getText().toString();
        } else {
            Log.i("rhl...", String.valueOf(rb1));

        }

        userImage.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        Dob.setOnClickListener(this);
        if (Utility.isNetAvailable(getActivity())){
            MyProfileApi(user_id);
        }else {
            Toast.makeText(getActivity(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void openDialogForPicture() {
        settingsDialog = new Dialog(getActivity());
        settingsDialog.setContentView(R.layout.image_source_dialog);
        settingsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        settingsDialog.setTitle("Choose your option..");
        LinearLayout dialogcamera = settingsDialog.findViewById(R.id.dialog_ll_camera);
        LinearLayout dialoggallery = settingsDialog.findViewById(R.id.dialog_ll_gallery);
        dialogcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, FROM_CAMERA);
                settingsDialog.dismiss();
            }
        });
        dialoggallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in_gallery = new Intent(Intent.ACTION_PICK);
                in_gallery.setType("image/*");
                in_gallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(in_gallery, FROM_GALLERY);
                settingsDialog.dismiss();
            }
        });
        settingsDialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userimage:
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;
            case R.id.selectbirthday_etv:
                final Calendar dob_date = Calendar.getInstance();
                final Calendar dob_currentDate = Calendar.getInstance();
                DatePickerDialog dob_atePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        dob_date.set(year, monthOfYear, dayOfMonth);
                        dobMonth = (monthOfYear + 1) + "";
                        dobDate = dayOfMonth + "";
                        dobYear = year + "";
                        Date date = dob_date.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        Dob.setText(dateFormat.format(date));

                    }
                }, dob_currentDate.get(Calendar.YEAR), dob_currentDate.get(Calendar.MONTH), dob_currentDate.get(Calendar.DATE));
                dob_date.add(Calendar.YEAR, -18);
                dob_atePickerDialog.getDatePicker().setMaxDate(dob_date.getTimeInMillis());
                dob_atePickerDialog.show();
                break;

            case R.id.SaveBtn:
               /* if (file == null) {
                    Utility.showToast(getActivity(), "Please upload your recent photo");
                    return;
                }
*/
                mUserName = fullName.getText().toString();
                mUserlastName = lastName.getText().toString();
                mUserEmail = lastName.getText().toString();
                mDob = Dob.getText().toString();
                mUserPassword = password.getText().toString();
                if (Utility.isNetAvailable(getActivity())){
                    updateprofileApi();
                }else {
                    Toast.makeText(getActivity(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private String getPathFromUri(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), photo, "Title", null);
            filePath = Uri.parse(path);
            pathProfile = getPathFromUri(filePath);
            file = new File(pathProfile);
            Glide.with(getActivity()).load(filePath).into(userImage);
        }
        if (requestCode == FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            filePath = data.getData();

            InputStream inputStream = null;
            try {
                inputStream = getActivity().getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "Title", null);
                filePath = Uri.parse(path);
                pathProfile = getPathFromUri(filePath);
                file = new File(pathProfile);
                Glide.with(getActivity()).load(filePath).into(userImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    private void requestCameraPermission() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA},
                CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestReadPermission() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_PERMISSION_REQUEST_CODE);
    }

    private void requestWritePermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                WRITE_PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private boolean checkWritePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private boolean checkReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;
            case READ_PERMISSION_REQUEST_CODE:
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;
            case WRITE_PERMISSION_REQUEST_CODE:
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                openDialogForPicture();
                break;
        }
    }

    private void updateprofileApi() {
       /* progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Uploading please wait...");
        progressDialog.show();*/
    /*    showDialog();
        MultipartBody.Part profile_picture;
        if (pathProfile == null || pathProfile.isEmpty()) {
            profile_picture = MultipartBody.Part.createFormData("user_img", "");

        } else {
            Log.e("imagePath...r", pathProfile);
            File file = new File(pathProfile);
            RequestBody fileProfile = MultipartBody.create(MediaType.parse("image/*"), file);
            profile_picture = MultipartBody.Part.createFormData("user_img", file.getName(), fileProfile);
        }
        // MultipartBody.Part profile_picture = MultipartBody.Part.createFormData("user_img", file.getName(), fileProfile);
        RequestBody useridPart = MultipartBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody usernamePart = MultipartBody.create(MediaType.parse("text/plain"), mUserName);
        RequestBody userEmailPart = MultipartBody.create(MediaType.parse("text/plain"), mUserEmail);
        RequestBody userPasswordPart = MultipartBody.create(MediaType.parse("text/plain"), mUserPassword);
        RequestBody usergenderPart = MultipartBody.create(MediaType.parse("text/plain"), s_radio2);
        RequestBody userMobilePart = MultipartBody.create(MediaType.parse("text/plain"), "123");
        RequestBody userDobPart = MultipartBody.create(MediaType.parse("text/plain"), mDob);
        RetrofitClient.getInterface().updateProfileApi(profile_picture, useridPart, usernamePart,
                userEmailPart, userPasswordPart, usergenderPart, userMobilePart, userDobPart)
                .enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        cancleDialog();
                        // progressDialog.dismiss();
                        Log.d("response", response.body().toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            if (jsonObject.getBoolean("response")) {
                                Utility.showToast(getActivity(), "Update Profile Successful");
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String user_id = jsonObject1.getString("user_id");
                                String user_name = jsonObject1.getString("user_name");
                                String email = jsonObject1.getString("user_email");
                                String userimage = jsonObject1.getString("user_img");
                                String user_pass = jsonObject1.getString("user_pass");
                                String userStatus = jsonObject1.getString("user_status");
                                String modified = jsonObject1.getString("modified");
                                String gender = jsonObject1.getString("gender");

                                SharedPrefsManager.getInstance().setString(KEY_USER_ID, user_id);
                                SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, userimage);
                                SharedPrefsManager.getInstance().setString(KEY_USER_NAME, user_name);
                                SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, email);

                                //  Log.e("gender", gender);
                                String dob = jsonObject1.getString("dob");
                                startActivity(new Intent(getActivity(), LandingScreenActivity.class));
                                getActivity().finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        //   Log.e("failuremsg", t.getMessage());
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                        cancleDialog();
                    }
                });*/
    }


    public void MyProfileApi(String user_id) {
        showDialog();
        apiInterface.MyProfileApi(user_id).enqueue(new Callback<MyProfileModel>() {
            @Override
            public void onResponse(Call<MyProfileModel> call, Response<MyProfileModel> response) {
                if (response.isSuccessful()) {
                    cancleDialog();
                    MyProfileModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        MyProfileModel.MyProfileListModel userdata1 = userdata.getData();
                        mUserName = userdata1.getUserName();
                        mUserEmail = userdata1.getUserEmail();
                        pathProfile = "";
                        // user_id = userdata1.getUserId();
                        mDob = userdata1.getDob();
                        mMobileNo = userdata1.getUserMobile();
                        s_radio2 = userdata1.getGender();
                        mUserPassword = userdata1.getUserPass();

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.usereditimage)
                                .error(R.drawable.usereditimage)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH).dontAnimate()
                                .dontTransform();

                        //  Glide.with(getActivity()).load(Constants.ImageBaseurl + userdata1.getUserImg()).apply(options).into(userImage);
                        fullName.setText(mUserName);
                        // lastName.setText(userdata1.getl);
                        email.setText(mUserEmail);
                        Dob.setText(mDob);
                        mobileNo.setText(mMobileNo);
                        password.setText(mUserPassword);
                        if (s_radio2.equalsIgnoreCase("female")) {
                            reg_woman.setChecked(true);
                        } else if (s_radio2.equalsIgnoreCase("male")) {
                            reg_man.setChecked(true);
                        } else {
                            reg_man.setChecked(true);
                        }
                    }
                } else {
                    cancleDialog();
                    Toast.makeText(getActivity(), "Something wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyProfileModel> call, Throwable t) {
                cancleDialog();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ProgressDailog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(getActivity()).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }
}
