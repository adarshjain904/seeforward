package com.audiobook.seeforward.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.CalendarActivity;
import com.audiobook.seeforward.activities.HelpandSupportActivity;
import com.audiobook.seeforward.activities.MyProgrammesActivity;
import com.audiobook.seeforward.profile.ProfileActivity;
import com.google.android.material.card.MaterialCardView;

public class DashboardFragment extends Fragment {

    private MaterialCardView profileView;
    private MaterialCardView programmesView;
    private MaterialCardView calendarView;
    private MaterialCardView helpFeedbackView;

    private Context mContext;

    private FragmentManager fragmentManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        fragmentManager = getFragmentManager();
        mContext = getContext();

        profileView = view.findViewById(R.id.dashboard_profile_view);
        programmesView = view.findViewById(R.id.dashboard_programmes_view);
        calendarView = view.findViewById(R.id.dashboard_calendar_view);
        helpFeedbackView = view.findViewById(R.id.dashboard_helpfeedback_view);

        profileView.setOnClickListener(view1 -> {
           /* FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new ViewProfileFragment()).addToBackStack(null);
            fragmentTransaction.commit();
            */
            Intent profileIntent = new Intent(mContext, ProfileActivity.class);
            startActivity(profileIntent);
        });

        programmesView.setOnClickListener(view12 -> {
           /* FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new MyPlanFragment()).addToBackStack(null);
            fragmentTransaction.commit();

            */
            Intent programmeIntent = new Intent(mContext, MyProgrammesActivity.class);
            startActivity(programmeIntent);
        });

        helpFeedbackView.setOnClickListener(view13 -> {
            Intent i = new Intent(mContext, HelpandSupportActivity.class);
            startActivity(i);
        });

        calendarView.setOnClickListener(view14 -> {
            Intent intent = new Intent(mContext, CalendarActivity.class);
            startActivity(intent);
        });

        return view;
    }
}