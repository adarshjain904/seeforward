package com.audiobook.seeforward.Fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.HelpModel;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.utiles.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpsFragment extends Fragment {

    WebView webView;
    RetrofitService apiInterface;

    public HelpsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_helps, container, false);
        initView(view);
        return view;
    }

    public void initView(View view) {
        webView = view.findViewById(R.id.webview);

        if (Utility.isNetAvailable(getActivity())){
            getHelpApi();
        }else {
            Toast.makeText(getActivity(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void getHelpApi() {
        apiInterface.getHelpApi().enqueue(new Callback<HelpModel>() {
            @Override
            public void onResponse(Call<HelpModel> call, Response<HelpModel> response) {
                Log.e("response", response.toString());
                if (response.isSuccessful()) {
                    HelpModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        HelpModel.HelpListModel userdata1 = userdata.getData();
                        String pageContent = userdata1.getPageContent();
                        webView.loadData(pageContent, "text/html", "UTF-8");
                    }
                }
            }

            @Override
            public void onFailure(Call<HelpModel> call, Throwable t) {
                Log.e("failureresponse", t.getMessage());
            }
        });

    }

}
