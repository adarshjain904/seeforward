package com.audiobook.seeforward;

import android.app.Application;
import android.os.Bundle;


import com.audiobook.seeforward.network.SharedPrefsManager;
import com.facebook.FacebookSdk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;

public class AppController extends Application {

    public static Gson mGson;
    public static Bundle bundle;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseCrashlytics mFirebaseAnalytics = FirebaseCrashlytics.getInstance();
        FacebookSdk.fullyInitialize();


        SharedPrefsManager.initialize(this);
    }

    public static Gson getGson() {
        if (mGson == null) {
            mGson = new Gson();
        }
        return mGson;
    }

    public static Bundle getBundle() {
        if(bundle == null) {
            bundle = new Bundle();
        }
        return bundle;
    }
}