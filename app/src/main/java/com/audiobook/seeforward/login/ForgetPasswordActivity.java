package com.audiobook.seeforward.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.ForgetPassword;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.utiles.Utility;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    private TextInputEditText emailEditText;
    private TextView submitBtn;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        ImageView backBtn = findViewById(R.id.back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());

        emailEditText = findViewById(R.id.email_edit_text);
        progressBar = findViewById(R.id.progress_bar);
        submitBtn = findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(view -> {
            String email = emailEditText.getText().toString().trim();

            if (!TextUtils.isEmpty(email)) {
                submitBtn.setEnabled(false);
                if (Utility.isNetAvailable(this)) {
                    submitEmail(email);
                } else {
                    Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void submitEmail(String email) {

        progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ForgetPassword> call = api.forgetPasswordEmail(email);
        call.enqueue(new Callback<ForgetPassword>() {
            @Override
            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {

                if (response.isSuccessful()) {
                    ForgetPassword forgetPassword = response.body();
                    progressBar.setVisibility(View.GONE);
                    submitBtn.setEnabled(true);
                    if (forgetPassword != null) {
                        if (forgetPassword.getResponse().equals("true")){
                            Toast.makeText(ForgetPasswordActivity.this, "Email sent, please check your mail", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                            Toast.makeText(ForgetPasswordActivity.this, "Please enter registered email", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                submitBtn.setEnabled(true);
            }
        });
    }
}