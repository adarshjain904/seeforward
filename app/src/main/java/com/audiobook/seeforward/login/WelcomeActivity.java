package com.audiobook.seeforward.login;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.audiobook.seeforward.network.RetrofitClient;
import com.bumptech.glide.Glide;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.WelcomeMsg;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.utiles.PrefManager;
import com.audiobook.seeforward.utiles.Utility;
import com.google.android.material.button.MaterialButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.next)
    MaterialButton next;
    private Dialog dialog;
    private PrefManager prefManager;
    WelcomeMsg welcomeMsg;
    int count = 0, totalCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this);

        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        ProgressDailog();

        if (Utility.isNetAvailable(this)) {
            if (Utility.isNetAvailable(this)) {
                getWelcome();
            } else {
                Toast.makeText(this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
        }
    }

    private void launchHomeScreen() {
        count++;
        setData(count);

    }

    private void getWelcome() {
        webView = findViewById(R.id.webview);
        webView.setBackgroundColor(Color.TRANSPARENT);

        showProgressDialog();
        // progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<WelcomeMsg> call = api.getWelcomeMsg();
        call.enqueue(new Callback<WelcomeMsg>() {
            @Override
            public void onResponse(Call<WelcomeMsg> call, Response<WelcomeMsg> response) {
                //Log.e("response", response.body().toString());

                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    welcomeMsg = response.body();
                    totalCount = welcomeMsg.getData().size() - 1;
                    setData(count);

                    cancleDialog();

                } else {
                    cancleDialog();
                    Toast.makeText(WelcomeActivity.this, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WelcomeMsg> call, Throwable t) {
                Toast.makeText(WelcomeActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                cancleDialog();
                // progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setData(int i) {
        if (count>totalCount){
            prefManager.setFirstTimeLaunch(false);
            startActivity(new Intent(WelcomeActivity.this, SelectionActivity.class));
            finish();
        }else {
            String content = welcomeMsg.getData().get(i).getPage_content();
            String styledText = "<font color='white'>" + content + "</font>";
            webView.loadData(styledText, "text/html", "UTF-8");
            if (count==totalCount)
                next.setText("Enter");

        }
    }

    private void ProgressDailog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(this).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showProgressDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }


    @Override
    public void onDestroy() {
        cancleDialog();
        super.onDestroy();
    }

    @OnClick(R.id.next)
    public void onViewClicked() {
        launchHomeScreen();
    }
}
