package com.audiobook.seeforward.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.PrefManager;
import com.audiobook.seeforward.utiles.Utility;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_STATUS;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.google.android.play.core.install.model.ActivityResult.RESULT_IN_APP_UPDATE_FAILED;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int HIGH_PRIORITY_UPDATE = 5;
    private static final int MY_REQUEST_CODE = 101;
    private PrefManager prefManager;
    AppUpdateManager appUpdateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);
        checkUpdate();
        hashkey();

    }

    private void checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE /*AppUpdateType.IMMEDIATE*/)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, this, MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    openActivity();
                    e.printStackTrace();
                }
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                //CHECK THIS if AppUpdateType.FLEXIBLE, otherwise you can skip
                popupSnackbarForCompleteUpdate();
            } else {
                openActivity();
            }
        });
    }

    private void openActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                prefManager = new PrefManager(SplashScreenActivity.this);
                if (!prefManager.isFirstTimeLaunch()) {
                    if (SharedPrefsManager.getInstance().getBoolean(KEY_LOGIN_STATUS)) {
                        if (Utility.isNetAvailable(SplashScreenActivity.this)) {
                            checkdays();
                        } else {
                            Toast.makeText(SplashScreenActivity.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Intent intent = new Intent(SplashScreenActivity.this, SelectionActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1500);
    }

    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.rl_spalsh), "New app is ready!", Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", view -> {
            if (appUpdateManager != null) {
                appUpdateManager.completeUpdate();
                openActivity();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    InstallStateUpdatedListener installStateUpdatedListener = new InstallStateUpdatedListener() {
        @Override
        public void onStateUpdate(InstallState state) {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackbarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                if (appUpdateManager != null) {
                    appUpdateManager.unregisterListener(installStateUpdatedListener);
                }
            } else {
                Log.e("InstallStateUpdated:", "" + state.installStatus());
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }
    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(SplashScreenActivity.this, LogInActivity.class));
        finish();
    }

    public void getProfile() {
        RetrofitClient.getClient().create(RetrofitService.class).MyProfileApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID)).enqueue(new Callback<MyProfileModel>() {
            @Override
            public void onResponse(Call<MyProfileModel> call, Response<MyProfileModel> response) {
                if (response.isSuccessful()) {
                    MyProfileModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        MyProfileModel.MyProfileListModel userdata1 = userdata.getData();
                        if (userdata1 != null) {
                            SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, userdata1.getScore());
                            SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, "0");
                            Intent intent = new Intent(SplashScreenActivity.this, LandingScreenActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MyProfileModel> call, Throwable t) {
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(SplashScreenActivity.this, "App download starts...", Toast.LENGTH_LONG).show();
            } else if (resultCode != RESULT_CANCELED) {
                Toast.makeText(SplashScreenActivity.this, "App download canceled.", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_IN_APP_UPDATE_FAILED) {
                Toast.makeText(SplashScreenActivity.this, "App download failed.", Toast.LENGTH_LONG).show();
            }
            openActivity();
        }


    }

    private void checkdays() {
        RetrofitClient.getInterface().checkShowQuestion(SharedPrefsManager.getInstance().getString(KEY_USER_ID)).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.d("response", response.body().toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    if (jsonObject.getBoolean("status")) {
                        SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, jsonObject.getString("data"));
                        Intent intent = new Intent(SplashScreenActivity.this, LandingScreenActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        getProfile();
                    }
                } catch (Exception e) {
                    getProfile();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
        // progressDialog.dismiss();
    }

    public void hashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.audiobook.seeforward",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("facebook NameNotFoundEx", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.e("fb NoSuchAlgorithmEx", e.getMessage());
        }
    }
}