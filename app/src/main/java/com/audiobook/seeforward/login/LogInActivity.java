package com.audiobook.seeforward.login;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.models.LoginModel;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_STATUS;
import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_TYPE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_EMAIL;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_NAME;

public class LogInActivity extends AppCompatActivity {
    @BindView(R.id.useremail)
    TextInputEditText userEmail;
    @BindView(R.id.userpassword)
    TextInputEditText userPassword;
    @BindView(R.id.LoginBtn)
    TextView LoginBtn;
    @BindView(R.id.signup)
    TextView signup;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.forget_password_btn)
    TextView forgetPasswordBtn;
    String mUserEmail, mUserPassword;
    private CallbackManager callbackManager;
    String userType="private_user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        printKeyHash();
        //adarsh 30/12 move this line above setContentView
        // callbackManager = CallbackManager.Factory.create();
        LoginButton facebookLoginButton = findViewById(R.id.facebook_login_button);

        facebookLoginButton.setPermissions(Arrays.asList("public_profile", "email"));

        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                if (isLoggedIn) {
                    getFacebookData(accessToken);
                }
            }
            @Override
            public void onCancel() {
                // App code
            }
            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        if (SharedPrefsManager.getInstance().getString("userType").equals("company")) {
            userType="company_user";
            facebookLoginButton.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
        }
    }

    public void loginApi(String username, String password) {
        progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<LoginModel> call = api.login(username, password,userType);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                Log.e("response", response.body().toString());
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    LoginModel loginModel = response.body();
                    String loginmessage = loginModel.getMessage();
                    if (loginModel.getResponse()) {
                        Toast.makeText(LogInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                        LoginModel.Userdata loginListModel = response.body().getUserdata();
                        SharedPrefsManager.getInstance().setBoolean(KEY_LOGIN_STATUS, true);
                        SharedPrefsManager.getInstance().setString(KEY_USER_ID, loginListModel.getUser_id());
                        SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, loginListModel.getUser_img());
                        SharedPrefsManager.getInstance().setString(KEY_USER_NAME, loginListModel.getUser_name());
                        SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, loginListModel.getUser_email());
                        SharedPrefsManager.getInstance().setString(KEY_LOGIN_TYPE, "other");

                        checkdays();
                       /* startActivity(new Intent(LogInActivity.this, LandingScreenActivity.class));
                        finish();*/
                    } else {
                        Toast.makeText(LogInActivity.this, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(LogInActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Toast.makeText(LogInActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void getProfile() {
        RetrofitClient.getClient().create(RetrofitService.class).MyProfileApi(SharedPrefsManager.getInstance().getString(KEY_USER_ID)).enqueue(new Callback<MyProfileModel>() {
            @Override
            public void onResponse(Call<MyProfileModel> call, Response<MyProfileModel> response) {
                if (response.isSuccessful()) {
                    MyProfileModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        MyProfileModel.MyProfileListModel userdata1 = userdata.getData();
                        if (userdata1 != null) {
                            SharedPrefsManager.getInstance().setString(Utility.PREF_SCORE, userdata1.getScore());
                            SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, "0");
                            Intent intent = new Intent(LogInActivity.this, LandingScreenActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MyProfileModel> call, Throwable t) {
            }
        });
    }

    @OnClick({R.id.LoginBtn, R.id.signup, R.id.forget_password_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.LoginBtn:
                mUserEmail = userEmail.getText().toString().trim();
                mUserPassword = userPassword.getText().toString().trim();
                if (TextUtils.isEmpty(mUserEmail)) {
                    userEmail.setError("Can't be Empty");
                    userEmail.setFocusable(true);
                } else if (TextUtils.isEmpty(mUserPassword)) {
                    userPassword.setError("Can't be Empty");
                    userPassword.setFocusable(true);
                } else {
                    if (Utility.isNetAvailable(LogInActivity.this)) {

                        loginApi(mUserEmail, mUserPassword);
                    } else {
                        Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.signup:
                startActivity(new Intent(LogInActivity.this, SignUpActivity.class));
                finish();
                break;

            case R.id.forget_password_btn:
                startActivity(new Intent(LogInActivity.this, ForgetPasswordActivity.class));
                break;

        }
    }

  /*  private void setupFacebook() {
        LoginManager loginManager = LoginManager.getInstance();

        loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        AccessToken accessToken = loginResult.getAccessToken();
                        if (accessToken != null) {
                            getFacebookData(accessToken);
                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getFacebookData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    // Insert your code here
                    try {
                        String facebookId = object.getString("id");
                        String name = object.getString("name");
                        String email = object.getString("email");

                        loginWithFacebook(facebookId,name, email, "", "");

                        //  Toast.makeText(this, name + email, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginWithFacebook(String facebookId,String name, String email, String mobile, String image) {

        progressBar.setVisibility(View.VISIBLE);
        RetrofitService api =   RetrofitClient.getClient().create(RetrofitService.class);

        api.loginWithFacebook(name, email,facebookId, mobile, image).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    LoginModel loginModel = response.body();
                    String loginmessage = loginModel.getMessage();
                    if (loginModel.getResponse()) {
                        Toast.makeText(LogInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                        LoginModel.Userdata loginListModel = response.body().getUserdata();
                        SharedPrefsManager.getInstance().setBoolean(KEY_LOGIN_STATUS, true);
                        SharedPrefsManager.getInstance().setString(KEY_USER_ID, loginListModel.getUser_id());
                        SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, loginListModel.getUser_img());
                        SharedPrefsManager.getInstance().setString(KEY_USER_NAME, loginListModel.getUser_name());
                        SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, loginListModel.getUser_email());
                        SharedPrefsManager.getInstance().setString(KEY_LOGIN_TYPE, "facebook");

                        //adarsh 30/12/20 use   getProfile(); method for getScore data
                        //addarsh 03/2/20 chage  getProfile(); to checkdays()
                        checkdays();
                       /* startActivity(new Intent(LogInActivity.this, LandingScreenActivity.class));
                        finish();*/
                    } else {
                        Toast.makeText(LogInActivity.this, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(LogInActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

            }
        });
    }


    private void checkdays() {
        RetrofitClient.getInterface().checkShowQuestion(SharedPrefsManager.getInstance().getString(KEY_USER_ID)).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.d("response", response.body().toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    if (jsonObject.getBoolean("status")) {
                        SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, jsonObject.getString("data"));
                        Intent intent = new Intent(LogInActivity.this, LandingScreenActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        getProfile();
                    }
                } catch (Exception e) {
                    getProfile();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
        // progressDialog.dismiss();
    }

    public String printKeyHash() {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}