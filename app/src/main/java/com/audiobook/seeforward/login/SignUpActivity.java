package com.audiobook.seeforward.login;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.audiobook.seeforward.activities.LandingScreenActivity;
import com.audiobook.seeforward.models.LoginModel;
import com.audiobook.seeforward.network.RetrofitService;
import com.bumptech.glide.Glide;
import com.audiobook.seeforward.R;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_STATUS;
import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_TYPE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_EMAIL;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_NAME;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    TextView signUpBtn, login;
    ImageView userimageview, selectimage, iv_back;
    EditText userFullname, userEmailId, userPassword, tv_employee_name, tv_role,tv_company_id, userComfirmpass, mobile;
    TextView signupContinueBtn;
    Dialog settingsDialog;
    public static int FROM_GALLERY = 1;
    public static int FROM_CAMERA = 2;
    String pathProfile;
    Uri filePath;
    Bitmap bitmap;
    File file;
    final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    final int READ_PERMISSION_REQUEST_CODE = 4;
    final int WRITE_PERMISSION_REQUEST_CODE = 5;
    private String s_profilepic = "";
    ProgressDialog progressDialog;
    LinearLayout ll_company;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();

        callbackManager = CallbackManager.Factory.create();
    }

    public void initView() {
        signUpBtn = findViewById(R.id.signUp_btn);
        userimageview = findViewById(R.id.user_image_view);
        selectimage = findViewById(R.id.select_image);
        userFullname = findViewById(R.id.username);
        userEmailId = findViewById(R.id.email);
        userPassword = findViewById(R.id.userpass);
        userComfirmpass = findViewById(R.id.confirmpass);
        tv_employee_name = findViewById(R.id.tv_employee_name);
        tv_role = findViewById(R.id.tv_role);
        tv_company_id = findViewById(R.id.tv_company_id);
        mobile = findViewById(R.id.mobile);
        iv_back = findViewById(R.id.iv_back);
        login = findViewById(R.id.login);
        ll_company = findViewById(R.id.ll_company);

        login.setOnClickListener(this);
        selectimage.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        /*if (SharedPrefsManager.getInstance().getString("userType").equals("company")) {
            ll_company.setVisibility(View.VISIBLE);
        }*/
    }


    private void openDialogForPicture() {
        settingsDialog = new Dialog(SignUpActivity.this);
        settingsDialog.setContentView(R.layout.image_source_dialog);
        settingsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        settingsDialog.setTitle("Choose your option..");
        LinearLayout dialogcamera = settingsDialog.findViewById(R.id.dialog_ll_camera);
        LinearLayout dialoggallery = settingsDialog.findViewById(R.id.dialog_ll_gallery);
        dialogcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, FROM_CAMERA);
                settingsDialog.dismiss();
            }
        });
        dialoggallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in_gallery = new Intent(Intent.ACTION_PICK);
                in_gallery.setType("image/*");
                in_gallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(in_gallery, FROM_GALLERY);
                settingsDialog.dismiss();
            }
        });
        settingsDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_image:
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;

            case R.id.signUp_btn:
                /*if (file == null) {
                    Utility.showToast(SignUpActivity.this, "Please upload your recent photo");
                    return;
                }*/

                if (validateInputs()) {
                    if (Utility.isNetAvailable(SignUpActivity.this)) {
                     /*   if (SharedPrefsManager.getInstance().getString("userType").equals("company")) {
                            signCompanyUpApi();
                        } else*/
                            signUpApi();
                    } else {
                        Toast.makeText(SignUpActivity.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                    }

                }
                break;

            case R.id.login:
                startActivity(new Intent(this, LogInActivity.class));
                finish();
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }

    private boolean validateInputs() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userFullname.getText().toString().trim())) {
            userFullname.setError("Cannot be empty");
            userFullname.requestFocus();
            isValid = false;

        } else if (TextUtils.isEmpty(userEmailId.getText().toString().trim())) {
            userEmailId.setError("Cannot be empty");
            userEmailId.requestFocus();
            isValid = false;

        } /*else if (TextUtils.isEmpty(mobile.getText().toString().trim())) {
            mobile.setError("Cannot be empty");
            mobile.requestFocus();
            return false;
        }*/ else if (TextUtils.isEmpty(userPassword.getText().toString().trim())) {
            userPassword.setError("Cannot be empty");
            userPassword.requestFocus();
            isValid = false;

        } else if (TextUtils.isEmpty(userComfirmpass.getText().toString().trim())) {
            userComfirmpass.setError("Cannot be empty");
            userComfirmpass.requestFocus();
            isValid = false;

        } else if (!userPassword.getText().toString().trim().equalsIgnoreCase(userComfirmpass.getText().toString().trim())) {
            Utility.showToast(SignUpActivity.this, "Password didn't matched");
            isValid = false;

        } else if (SharedPrefsManager.getInstance().getString("userType").equals("company")) {
            if (TextUtils.isEmpty(tv_employee_name.getText().toString().trim())) {
                tv_employee_name.setError("Cannot be empty");
                tv_employee_name.requestFocus();
                isValid = false;

            } else if (TextUtils.isEmpty(tv_company_id.getText().toString().trim())) {
                tv_company_id.setError("Cannot be empty");
                tv_company_id.requestFocus();
                isValid = false;

            }else if (TextUtils.isEmpty(tv_role.getText().toString().trim())) {
                tv_role.setError("Cannot be empty");
                tv_role.requestFocus();
                isValid = false;

            }
        }
        return isValid;
    }

    private String getPathFromUri(Uri uri) {
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            String path = MediaStore.Images.Media.insertImage(SignUpActivity.this.getContentResolver(), photo, "Title", null);
            filePath = Uri.parse(path);
            pathProfile = getPathFromUri(filePath);
            file = new File(pathProfile);
            Glide.with(this).load(filePath).into(userimageview);
        }


        if (requestCode == FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            filePath = data.getData();

            InputStream inputStream = null;
            try {
                inputStream = SignUpActivity.this.getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                String path = MediaStore.Images.Media.insertImage(SignUpActivity.this.getContentResolver(), bitmap, "Title", null);
                filePath = Uri.parse(path);
                pathProfile = getPathFromUri(filePath);
                file = new File(pathProfile);
                Glide.with(this).load(filePath).into(userimageview);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    private void requestCameraPermission() {

        ActivityCompat.requestPermissions(SignUpActivity.this,
                new String[]{Manifest.permission.CAMERA},
                CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestReadPermission() {

        ActivityCompat.requestPermissions(SignUpActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_PERMISSION_REQUEST_CODE);
    }

    private void requestWritePermission() {
        ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                WRITE_PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private boolean checkWritePermission() {
        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private boolean checkReadPermission() {
        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;
            case READ_PERMISSION_REQUEST_CODE:
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                if (!checkWritePermission()) {
                    requestWritePermission();
                    return;
                }
                openDialogForPicture();
                break;
            case WRITE_PERMISSION_REQUEST_CODE:
                if (!checkReadPermission()) {
                    requestReadPermission();
                    return;
                }
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                    return;
                }
                openDialogForPicture();
                break;
        }
    }

    public void signUpApi() {
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Uploading please wait...");
        progressDialog.show();

        MultipartBody.Part profile_picture;
        if (file == null) {
            profile_picture = MultipartBody.Part.createFormData("user_img", "");
        } else {
            RequestBody fileProfile = MultipartBody.create(MediaType.parse("image/*"), file);
            profile_picture = MultipartBody.Part.createFormData("user_img", file.getName(), fileProfile);
        }

        RequestBody usernamePart = MultipartBody.create(MediaType.parse("text/plain"), userFullname.getText().toString());
        RequestBody userEmailPart = MultipartBody.create(MediaType.parse("text/plain"), userEmailId.getText().toString());
        RequestBody userPasswordPart = MultipartBody.create(MediaType.parse("text/plain"), userPassword.getText().toString());
        //RequestBody usercomfirmPasswordPart = MultipartBody.create(MediaType.parse("text/plain"), mUserConfirmPassword);

        RequestBody userMobilePart = MultipartBody.create(MediaType.parse("text/plain"), "1234");
        RetrofitClient.getInterface().submitRegister(profile_picture, usernamePart, userEmailPart, userPasswordPart, userMobilePart)
                .enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            if (jsonObject.getBoolean("response")) {
                                Utility.showToast(SignUpActivity.this, "Registration Successful");
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String email = jsonObject1.getString("user_email");
                                SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, jsonObject1.getString("user_img"));
                                SharedPrefsManager.getInstance().setString(KEY_USER_NAME, jsonObject1.getString("user_name"));
                                SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, jsonObject1.getString("user_email"));

                                SharedPrefsManager.getInstance().setString(KEY_LOGIN_TYPE, "other");

                                String user_id = jsonObject1.getString("user_id");
                                SharedPrefsManager.getInstance().setBoolean(KEY_LOGIN_STATUS, true);
                                SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, "0");
                                SharedPrefsManager.getInstance().setString(KEY_USER_ID, user_id);
                                Intent intent = new Intent(SignUpActivity.this, LandingScreenActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(SignUpActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Log.e("failuremsg", t.getMessage());
                    }
                });
    }


   /* public void signCompanyUpApi() {
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Uploading please wait...");
        progressDialog.show();

        MultipartBody.Part profile_picture;
        if (file == null) {
            profile_picture = MultipartBody.Part.createFormData("user_img", "");
        } else {
            RequestBody fileProfile = MultipartBody.create(MediaType.parse("image/*"), file);
            profile_picture = MultipartBody.Part.createFormData("user_img", file.getName(), fileProfile);
        }

        RequestBody usernamePart = MultipartBody.create(MediaType.parse("text/plain"), userFullname.getText().toString());
        RequestBody userEmailPart = MultipartBody.create(MediaType.parse("text/plain"), userEmailId.getText().toString());
        RequestBody userPasswordPart = MultipartBody.create(MediaType.parse("text/plain"), userPassword.getText().toString());
        RequestBody userMobilePart = MultipartBody.create(MediaType.parse("text/plain"), "1234");
        RequestBody employeeName = MultipartBody.create(MediaType.parse("text/plain"), tv_employee_name.getText().toString());
        RequestBody employeeRole = MultipartBody.create(MediaType.parse("text/plain"), tv_role.getText().toString());
        RequestBody uniqueNumber = MultipartBody.create(MediaType.parse("text/plain"), tv_company_id.getText().toString());
        //RequestBody usercomfirmPasswordPart = MultipartBody.create(MediaType.parse("text/plain"), mUserConfirmPassword);


        RetrofitClient.getInterface().comapnysignupRegister(profile_picture,
                usernamePart, userEmailPart, userPasswordPart, userMobilePart,employeeName,uniqueNumber,employeeRole)
                .enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            if (jsonObject.getBoolean("response")) {
                                Utility.showToast(SignUpActivity.this, "Registration Successful");
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String email = jsonObject1.getString("user_email");
                                SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, jsonObject1.getString("user_img"));
                                SharedPrefsManager.getInstance().setString(KEY_USER_NAME, jsonObject1.getString("user_name"));
                                SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, jsonObject1.getString("user_email"));

                                SharedPrefsManager.getInstance().setString(KEY_LOGIN_TYPE, "other");

                                String user_id = jsonObject1.getString("user_id");
                                SharedPrefsManager.getInstance().setBoolean(KEY_LOGIN_STATUS, true);
                                SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, "0");
                                SharedPrefsManager.getInstance().setString(KEY_USER_ID, user_id);
                                Intent intent = new Intent(SignUpActivity.this, LandingScreenActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(SignUpActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Log.e("failuremsg", t.getMessage());
                    }
                });
    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SignUpActivity.this, LogInActivity.class);
        startActivity(intent);
        finish();
    }

    private void setupFacebook() {
        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        AccessToken accessToken = loginResult.getAccessToken();
                        if (accessToken != null) {
                            getFacebookData(accessToken);
                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    private void getFacebookData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    // Insert your code here
                    try {
                        String facebookId = object.getString("id");
                        String name = object.getString("name");
                        String email = object.getString("email");

                        if (Utility.isNetAvailable(SignUpActivity.this)) {
                            loginWithFacebook(facebookId, name, email, "", "");
                        } else {
                            Toast.makeText(SignUpActivity.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                        }


//                        Toast.makeText(this, name + email, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginWithFacebook(String facebookId, String name, String email, String mobile, String image) {

        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);

        api.loginWithFacebook(name, email, facebookId, mobile, image).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body() != null) {
                    //  Boolean status = response.body().getResponse();
                    LoginModel loginModel = response.body();
                    String loginmessage = loginModel.getMessage();
                    if (loginModel.getResponse()) {
                        Toast.makeText(SignUpActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                        LoginModel.Userdata loginListModel = response.body().getUserdata();
                        SharedPrefsManager.getInstance().setBoolean(KEY_LOGIN_STATUS, true);
                        SharedPrefsManager.getInstance().setString(Utility.PREF_DAY_DATA, "0");
                        SharedPrefsManager.getInstance().setString(KEY_USER_ID, loginListModel.getUser_id());
                        SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, loginListModel.getUser_img());
                        SharedPrefsManager.getInstance().setString(KEY_USER_NAME, loginListModel.getUser_name());
                        SharedPrefsManager.getInstance().setString(KEY_USER_EMAIL, loginListModel.getUser_email());
                        SharedPrefsManager.getInstance().setString(KEY_LOGIN_TYPE, "facebook");

                        startActivity(new Intent(SignUpActivity.this, LandingScreenActivity.class));
                        finish();
                    } else {
                        Toast.makeText(SignUpActivity.this, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

            }
        });
    }
}
