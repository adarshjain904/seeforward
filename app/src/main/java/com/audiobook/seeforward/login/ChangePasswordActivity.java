package com.audiobook.seeforward.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.models.ChangePassword;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;

public class ChangePasswordActivity extends AppCompatActivity {

    private TextInputEditText oldPasswordEditText;
    private TextInputEditText newPasswordEditText;
    private TextInputEditText confirmPasswordEditText;

    private ProgressBar progressBar;

    private TextView submitBtn;

    private String mUserId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ImageView backBtn = findViewById(R.id.back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());

        mUserId = SharedPrefsManager.getInstance().getString(KEY_USER_ID);

        initViews();
    }

    private void initViews() {
        progressBar = findViewById(R.id.progress_bar);
        oldPasswordEditText = findViewById(R.id.old_password_edit_text);
        newPasswordEditText = findViewById(R.id.new_password_edit_text);
        confirmPasswordEditText = findViewById(R.id.confirm_password_edit_text);

        submitBtn = findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(view -> {

            String oldPassword = oldPasswordEditText.getText().toString().trim();
            String newPassword = newPasswordEditText.getText().toString().trim();
            String confirmPassword = confirmPasswordEditText.getText().toString().trim();

            boolean isValidForm = validatePasswordForm(newPassword, confirmPassword);

            if (isValidForm) {
                submitBtn.setEnabled(false);
                if (Utility.isNetAvailable(this)) {
                    changePassword(mUserId, oldPassword, newPassword, confirmPassword);
                } else {
                    Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void changePassword(String userId, String oldPassword, String newPassword, String confirmPassword) {

        progressBar.setVisibility(View.VISIBLE);
        RetrofitService api = RetrofitClient.getClient().create(RetrofitService.class);
        Call<ChangePassword> call = api.changePassword(oldPassword, newPassword, confirmPassword, userId);

        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {

                if (response.isSuccessful()) {
                    ChangePassword changePassword = response.body();
                    submitBtn.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    if (changePassword != null) {
                        if (changePassword.getResponse().equals("true")) {
                            Toast.makeText(ChangePasswordActivity.this, "Password successfully changed", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, "Wrong old password", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                submitBtn.setEnabled(true);
            }
        });
    }

    private boolean validatePasswordForm(String newPassword, String confirmPassword) {
        if (TextUtils.isEmpty(newPassword)) {
            newPasswordEditText.setError("Cannot be empty");
            newPasswordEditText.requestFocus();
            return false;
        } else if (!newPassword.equals(confirmPassword)) {
            confirmPasswordEditText.setError("Password didn't match");
            confirmPasswordEditText.requestFocus();
            return false;
        } else {
            return true;
        }
    }
}