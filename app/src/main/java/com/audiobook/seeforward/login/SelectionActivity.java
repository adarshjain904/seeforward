package com.audiobook.seeforward.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;

public class SelectionActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_private, tv_company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_type);
        init();
    }

    private void init() {
        tv_company = findViewById(R.id.tv_company);
        tv_private = findViewById(R.id.tv_private);

        tv_company.setOnClickListener(this);
        tv_private.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_private:
                clickIntent("private");
                break;
            case R.id.tv_company:
                clickIntent("company");
                break;
        }
    }

    public void clickIntent(String userType){
        SharedPrefsManager.getInstance().setString("userType", userType);
        Intent intent=new Intent(this,LogInActivity.class);
        startActivity(intent);

    }
}
