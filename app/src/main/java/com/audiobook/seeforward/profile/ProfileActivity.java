package com.audiobook.seeforward.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.audiobook.seeforward.R;
import com.audiobook.seeforward.login.ChangePasswordActivity;
import com.audiobook.seeforward.models.MyProfileModel;
import com.audiobook.seeforward.network.Constants;
import com.audiobook.seeforward.network.RetrofitClient;
import com.audiobook.seeforward.network.RetrofitService;
import com.audiobook.seeforward.network.SharedPrefsManager;
import com.audiobook.seeforward.utiles.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.audiobook.seeforward.network.Constants.KEY_LOGIN_TYPE;
import static com.audiobook.seeforward.network.Constants.KEY_USER_ID;
import static com.audiobook.seeforward.network.Constants.KEY_USER_IMAGE;

public class ProfileActivity extends AppCompatActivity {

    TextView editprofile;
    ImageView userImage;
    TextView userName, userEmail, country, region,
            branch, location, role, department, company;
    RetrofitService apiInterface;
    String user_id;
    LinearLayout ll_company;
    private Dialog dialog;

    private String mLoginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ImageView backBtn = findViewById(R.id.back_btn);
        backBtn.setOnClickListener(v -> onBackPressed());

        apiInterface = RetrofitClient.getClient().create(RetrofitService.class);
        user_id = SharedPrefsManager.getInstance().getString(KEY_USER_ID);
        mLoginType = SharedPrefsManager.getInstance().getString(KEY_LOGIN_TYPE);

        ProgressDailog();

        init();

        if (Utility.isNetAvailable(this)) {
            MyProfileApi(user_id);
        } else {
            Toast.makeText(this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }



    }

    public void init() {
        editprofile = findViewById(R.id.edit_profile);
        ll_company = findViewById(R.id.ll_company);
        userName = findViewById(R.id.username);
        userEmail = findViewById(R.id.useremail);
        userImage = findViewById(R.id.userimage);
        country = findViewById(R.id.country);
        region = findViewById(R.id.region);
        branch = findViewById(R.id.branch);
        location = findViewById(R.id.location);
        role = findViewById(R.id.role);
        department = findViewById(R.id.department);
        company = findViewById(R.id.company);

        TextView changePasswordBtn = findViewById(R.id.change_password_btn);

        if (mLoginType != null && mLoginType.equals("facebook")) {
            changePasswordBtn.setVisibility(View.GONE);
        } else {
            changePasswordBtn.setVisibility(View.VISIBLE);
        }

        if (SharedPrefsManager.getInstance().getString("userType").equals("company")) {
            editprofile.setVisibility(View.GONE);
        }else {
            ll_company.setVisibility(View.GONE);
        }


        changePasswordBtn.setOnClickListener(view1 -> {
            Intent intent = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
            startActivity(intent);
        });

        editprofile.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
            startActivity(intent);
        });
    }

    private void MyProfileApi(String user_id) {
        showDialog();
        apiInterface.MyProfileApi(user_id).enqueue(new Callback<MyProfileModel>() {
            @Override
            public void onResponse(Call<MyProfileModel> call, Response<MyProfileModel> response) {
                if (response.isSuccessful()) {
                    cancleDialog();
                    MyProfileModel userdata = response.body();
                    String apiresponse = userdata.getResponse();
                    String message = userdata.getMessage();
                    if (apiresponse.equals("true")) {
                        MyProfileModel.MyProfileListModel userdata1 = userdata.getData();

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.usereditimage)
                                .error(R.drawable.usereditimage)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH).dontAnimate()
                                .dontTransform();
                        SharedPrefsManager.getInstance().setString(KEY_USER_IMAGE, userdata1.getUserImg());
                        Glide.with(ProfileActivity.this).load(Constants.ImageBaseurl + userdata1.getUserImg()).apply(options).into(userImage);
                        userName.setText(userdata1.getUserName());
                        userEmail.setText(userdata1.getUserEmail());
                        country.setText(userdata1.getUserCountry());
                        region.setText(userdata1.getUserRegion());
                        branch.setText(userdata1.getUserBranch());
                        location.setText(userdata1.getUserLocation());
                        role.setText(userdata1.getUserRole());
                        department.setText(userdata1.getUserDepartment());
                        company.setText(userdata1.getUserCompany());
                    }
                } else {
                    cancleDialog();
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_something_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyProfileModel> call, Throwable t) {
                cancleDialog();
                Toast.makeText(ProfileActivity.this, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ProgressDailog() {
        dialog = new Dialog(ProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.prograss_bar_dialog);
        ImageView loder = (ImageView) dialog.findViewById(R.id.loder);
        Glide.with(ProfileActivity.this).load(R.drawable.loder).into(loder);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    //Show Progress Dialog...
    private void showDialog() {
        dialog.show();
    }

    //Cancle Progress Dialog...
    private void cancleDialog() {
        dialog.dismiss();
    }
}

